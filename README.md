# README #
Informe:

* Caratula

* Introducción: debemos dar una explicación para que sirve nuestro programa y dar un pequeño resumen de los conceptos que utilizamos para el programa (Que es un código bloque, entropía...etc.)

* Manual del usuario

* Conclusiones

 
 
 

Funciones a desarrollar:


1.Determinar la entropía o falta de información asociada a una distribución de probabilidades:

*  La probabilidad de cada evento posible.

*  Unidad del resultado.

2.Compara la entropía o falta de información asociada a una distribución de probabilidades con la falta de información de la equiprobabilidad.

 * La probabilidad de cada evento posible.

 * Unidad del resultado.


3.Determinar que código es más eficiente entre otros:

 * Por cada código se va a necesitar:

      * * Alfabeto fuente.

      * * Probabilidad de cada símbolo del alfabeto fuente.

      * * Palabra código de cada símbolo del alfabeto.


4.Determinar la longitud media de un código:

 * Alfabeto fuente.

 * Probabilidad de cada símbolo del alfabeto fuente.


5.A partir de un código bloque determinar qué propiedades cumple:

 * Si es singular o no lo es

 * Si es unívocamente decodificado o no.

 * Si es un código instantáneo o no.


6.Devolver un código bloque instantáneo a partir de:

 * Alfabeto fuente que se quiere codificar.

 * Alfabeto código.

 * Longitudes de las palabras códigos.

 * Si alguna de las palabras código tienen una valor específico se tendrá en cuenta.