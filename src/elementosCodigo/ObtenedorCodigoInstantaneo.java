package elementosCodigo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class ObtenedorCodigoInstantaneo {

    private CodigoBloque codigo;
    private List<String> codigosPosibles;
    private Iterator<String> siguienteCodigoDisponible;

    public class ConjuntoPalabraFuenteProbabilidad {
        double probabilidad;
        String palabraFuente;

        public ConjuntoPalabraFuenteProbabilidad(double probabilidad, String palabraFuente) {
            this.probabilidad = probabilidad;
            this.palabraFuente = palabraFuente;
        }
    }

    public ObtenedorCodigoInstantaneo() {
        codigo = new CodigoBloque();
        codigosPosibles = null;
        siguienteCodigoDisponible = null;
    }

    @SuppressWarnings("unchecked")
    private List<ConjuntoPalabraFuenteProbabilidad> crearListaALfabetoFuenteProbabilidad(
            List<Double> probabilidades, List<String> alfabetoFuente) {
        List<ConjuntoPalabraFuenteProbabilidad> datos = new ArrayList<ConjuntoPalabraFuenteProbabilidad>();
        int i = 0;
        for (Double proba : probabilidades) {
            datos.add(new ConjuntoPalabraFuenteProbabilidad(proba, alfabetoFuente.get(i)));
            i++;
        }
        // Ordenarlo de menor a mayor
        Collections.sort(datos, new Comparator() {

            public int compare(ConjuntoPalabraFuenteProbabilidad p1, ConjuntoPalabraFuenteProbabilidad p2) {
                return new Double(p2.probabilidad).compareTo(new Double(p1.probabilidad));
            }

            @Override
            public int compare(Object o1, Object o2) {
                return this.compare((ConjuntoPalabraFuenteProbabilidad) o1,
                        (ConjuntoPalabraFuenteProbabilidad) o2);
            }
        });
        return datos;
    }
    
    public CodigoBloque armarCodigo(List<Integer> tamanioLongitudes, List<String> alfabetoFuente,
            List<String> alfabetoCodigo, List<Double> probabilidades) {
        Kraft kraft = new Kraft();
        if (!kraft.cumple(tamanioLongitudes, alfabetoCodigo.size()))
            return null;
        List<ConjuntoPalabraFuenteProbabilidad> palabraFuenteProba = crearListaALfabetoFuenteProbabilidad(probabilidades, alfabetoFuente);
        Collections.sort(tamanioLongitudes);
        int longitudAnterior = 0;
        int i=0;
        for (Integer longitud : tamanioLongitudes) {
            String palabraCodigo = obtenerCodigo(alfabetoCodigo, longitud,
                    longitudAnterior);
            ConjuntoPalabraFuenteProbabilidad conjunto= palabraFuenteProba.get(i);
            codigo.agregarUnidad(conjunto.palabraFuente, palabraCodigo, conjunto.probabilidad);
            longitudAnterior = longitud;
            i++;
        }
        return codigo;
    }

    private String obtenerCodigo(List<String> alfabetoCodigo, Integer longitud, int longitudAnterior) {
        if (longitud != longitudAnterior) {
            // Creo la lista con los posibles codigos
            obtenerCodigosPosibles(alfabetoCodigo, longitud - longitudAnterior);
            return siguienteCodigoDisponible.next();
        }
        return siguienteCodigoDisponible.next();
    }

    // siguienteCodigo+codigo1+codigo2
    private void obtenerCodigosPosibles(List<String> alfabetoCodigo, int cantidad) {
        // Guardo el prefijo que vamos a usar
        String prefijo = "";
        if (siguienteCodigoDisponible != null)
            prefijo = siguienteCodigoDisponible.next();
        // Creo una nueva lista de codigos posibles
        codigosPosibles = new ArrayList<String>();
        int cantidadAlfabeto = alfabetoCodigo.size();
        List<Integer> listaDeIteradores = obtenerListaDeIteradores(alfabetoCodigo, cantidad);
        for (int i = 0; i < (cantidadAlfabeto * cantidad); i++) {
            String codigo1 = obtenerSiguienteCodigo(listaDeIteradores, alfabetoCodigo);
            codigosPosibles.add(prefijo + codigo1);
        }
        siguienteCodigoDisponible = codigosPosibles.iterator();
    }

    private List<Integer> obtenerListaDeIteradores(List<String> alfabetoCodigo, int cantidad) {
        List<Integer> listaDeIteradores = new ArrayList<Integer>();
        for (int i = 0; i < cantidad; i++)
            listaDeIteradores.add(0);
        return listaDeIteradores;
    }

    private String obtenerSiguienteCodigo(List<Integer> listaDeIteradores,
            List<String> alfabetoCodigo) {
        String resultado = "";
        int i = 0;
        int num = 0;
        // Agrego la informacion
        for (int j = listaDeIteradores.size() - 1; j >= 0; j--) {
            int iterador = listaDeIteradores.get(j);
            resultado += alfabetoCodigo.get(iterador);
        }
        // Aumento en 1
        int iterador = listaDeIteradores.get(0);
        listaDeIteradores.set(0, iterador + 1);
        do {
            num = listaDeIteradores.get(i);
            if (num >= alfabetoCodigo.size()) {
                listaDeIteradores.set(i, 0);
                if ((i + 1) < listaDeIteradores.size()) {
                    listaDeIteradores.set(i + 1, listaDeIteradores.get(i + 1) + 1);
                }
            }
            i++;
        } while ((num >= alfabetoCodigo.size()) && (i < listaDeIteradores.size()));
        return resultado;
    }
}
