package controlador;

import java.util.List;

public interface Menu {
	
	/**
	 * Devuelve un string con la entropia del codigo
	 */
	public String calcularEntropia(List<Double> probabilidades, String unidad);
	
	/**
	 * Va a devolver un string donde explique cual es preferible y los valores de la entropia
	 * */
	public String compararEntropiaConUnaDistribucionEquiprobable(List<Double> probabilidades, String unidad);
		
	/**
	 * Va a devolver un string donde explique cual es el mas eficiente y el porque
	 * */
	//public String determinarCodigoMasEficiente(List<ArrayList<Double>> listasProbasCodigos, List<ArrayList<String>> listasAlfabetos, List<ArrayList<String>> listasPalabras);
	
	/**
	 * Devuelve un String con las longitudes de cada palabra codigo y la longitud media del codigo
	 */
	public String determinarLongitudMedia( List<String> alfabeto,  List<Double> probabilidades);

	/**
	 * Va a devolver un string donde explique que propiedades cumple el codigo
	 * */
	public String propiedadesCodigoBloque(List<Double> listaProbabilidades, List<String> listaAlfabetoFuente, List<String> listaPalabrasCodigo, List<String> listaAlfabetoCodigo);

	
	/**
	 * Devuelve un codigo instantaneo con las condiciones que se paso por parametro
	 */
	public String devolverCodigoInstantaneo(List<String> alfabetoFuente, List<String> alfabetoCodigo, List<Integer> longitudes, List<Double> probabilidades);

}
