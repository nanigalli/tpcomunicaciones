import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import elementosCodigo.ObtenedorDePrefijos;
import elementosCodigo.UnidadCodigoBloque;

public class UnidadCodigoBloqueTest {

	/*
	 * @Test(expected = NullPointerException.class) public void
	 * testUnidadConNulls(){ new UnidadCodigoBloque(null, null, null); }
	 */

//	@Test
//	public void testComprararUnidadesPorCodigo() {
//		UnidadCodigoBloque unidad1 = new UnidadCodigoBloque("a", "b", 0);
//		UnidadCodigoBloque unidad2 = new UnidadCodigoBloque("x", "y", 0);
//		assertTrue(unidad1.sonIgualesLosCodigos(unidad2));
//	}
//
//	@Test
//	public void testComprararUnidadesPorCodigoDistinto() {
//		UnidadCodigoBloque unidad1 = new UnidadCodigoBloque("a", "b", 0);
//		UnidadCodigoBloque unidad2 = new UnidadCodigoBloque("x", "y", 0);
//		assertFalse(unidad1.sonIgualesLosCodigos(unidad2));
//	}

	@Test
	public void testComprararUnidadesPorPalabraCodigo() {
		UnidadCodigoBloque unidad1 = new UnidadCodigoBloque("a", "b", 0);
		UnidadCodigoBloque unidad2 = new UnidadCodigoBloque("x", "b", 0);
		assertTrue(unidad1.sonIgualesLasPalabrasCodigos(unidad2));
	}

	@Test
	public void testComprararUnidadesPorPalabraCodigoDistinto() {
		UnidadCodigoBloque unidad1 = new UnidadCodigoBloque("a", "b", 0);
		UnidadCodigoBloque unidad2 = new UnidadCodigoBloque("x", "y", 0);
		assertFalse(unidad1.sonIgualesLasPalabrasCodigos(unidad2));
	}

	@Test
	public void testComprararUnidadYStringPorPalabraCodigo() {
		UnidadCodigoBloque unidad1 = new UnidadCodigoBloque("a", "b", 0);
		assertTrue(unidad1.sonIgualesLasPalabrasCodigos("b"));
	}

	@Test
	public void testComprararUnidadYStringPorPalabraCodigoDistinto() {
		UnidadCodigoBloque unidad1 = new UnidadCodigoBloque("a", "b", 0);
		assertFalse(unidad1.sonIgualesLasPalabrasCodigos("y"));
	}

	@Test
	public void testComprararUnidadesPorSimboloFuente() {
		UnidadCodigoBloque unidad1 = new UnidadCodigoBloque("a", "b", 0);
		UnidadCodigoBloque unidad2 = new UnidadCodigoBloque("a", "y", 0);
		assertTrue(unidad1.sonIgualesLosSimbolosFuentes(unidad2));
	}

	@Test
	public void testComprararUnidadesPorSimboloFuenteDistinto() {
		UnidadCodigoBloque unidad1 = new UnidadCodigoBloque("a", "b", 0);
		UnidadCodigoBloque unidad2 = new UnidadCodigoBloque("x", "y", 0);
		assertFalse(unidad1.sonIgualesLosSimbolosFuentes(unidad2));
	}

	@Test
	public void testObtenerPrefijo() {
		UnidadCodigoBloque unidad = new UnidadCodigoBloque("a", "choco", 0);
		List<String> prefijos = unidad.obtenerPrefijosPalabraCodigo();
		List<String> prefijosCalculados = new ArrayList<String>();
		prefijosCalculados.add("c");
		prefijosCalculados.add("ch");
		prefijosCalculados.add("cho");
		prefijosCalculados.add("choc");
		assertTrue(prefijos.containsAll(prefijosCalculados));
	}

}
