package controlador;

import java.util.List;
import java.util.Map;

import elementosCodigo.CodigoBloque;
import elementosCodigo.ObtenedorCodigoInstantaneo;

public class MenuImplementacion implements Menu {

//    private double unidadPorDefecto = 2;

    /**
     * Valida que la lista de probabilidades de 1
     * @param probabilidades : lista de probabilidades
     * @return true si la suma de las probabilidades da 1, en caso contrario devuelve false
     */
    private boolean validarProbabilidades(List<Double> probabilidades) {
        double sumProbabilidades = 0;
        for (double proba : probabilidades) {
            sumProbabilidades += proba;
        }
        return (sumProbabilidades == 1);
    }

    /**
     * Devuelve un mesaje indicando que las probabilidades son invalidas
     */
    private String mensajeProbabilidadesInvalidas() {
        return "La suma de probabilidades no da 1";
    }

    /**
     * Valida que los strings de la lista no sean nulos
     * 
     * @param lista con strings
     * @return true si la lista no tiene nulos, en caso contrario false
     */
    private boolean validarListasStringNoNulas(List<String> lista) {
        for (String elem : lista) {
            if (elem == null)
                return false;
        }
        return true;
    }

    /**
     * Devuelve un mesaje indicando que la lista tiene valores nulos
     */
    private String mensajeListasConNulos(String lista) {
        return "La lista de " + lista + " no puede tener elementos vacios.";
    }

    /**
     * Devuelve la base de la unidad.
     */
    private double obtenerBase(String unidad) {
        if (unidad.equals("bits"))
            return 2;
        if (unidad.equals("base e"))
            return Math.E;
        // Le devolvemos el valor de la base sin la palabra "base "
        return Double.parseDouble(unidad.substring("base ".length()));
    }

    @Override
    public String calcularEntropia(List<Double> probabilidades, String unidad) {
        if (!validarProbabilidades(probabilidades))
            return mensajeProbabilidadesInvalidas();
        CodigoBloque codigoBloque = new CodigoBloque();
        for (double proba : probabilidades) {
            codigoBloque.agregarUnidad(null, null, proba);
        }
        return "" + codigoBloque.devolverEntropia(obtenerBase(unidad)) + " " + unidad;
    }

    @Override
    public String compararEntropiaConUnaDistribucionEquiprobable(List<Double> probabilidades,
            String unidad) {
        if (probabilidades.size() == 0)
            return "La lista de probabilidades debe tener al menos un valor";
        if (!validarProbabilidades(probabilidades))
            return mensajeProbabilidadesInvalidas();
        CodigoBloque codigoBloqueNoEquiprobable = new CodigoBloque();
        CodigoBloque codigoBloqueEquiprobable = new CodigoBloque();
        double probabilidadEquiprobable = Double
                .valueOf(1 / Double.valueOf((probabilidades.size())));
        for (Double probabilidad : probabilidades) {
            codigoBloqueNoEquiprobable.agregarUnidad(null, null, probabilidad);
            codigoBloqueEquiprobable.agregarUnidad(null, null, probabilidadEquiprobable);
        }

        double entropiaNoEquiprobable = codigoBloqueNoEquiprobable
                .devolverEntropia(obtenerBase(unidad));
        double entropiaEquiprobable = codigoBloqueEquiprobable
                .devolverEntropia(obtenerBase(unidad));

        return obtenerSignificadoCompararEntropias(entropiaNoEquiprobable, entropiaEquiprobable, unidad);
    }

//    @Override
//    public String determinarCodigoMasEficiente(List<ArrayList<Double>> listasProbasCodigos,
//            List<ArrayList<String>> listasAlfabetos, List<ArrayList<String>> listasPalabras) {
//        for (ArrayList<Double> probas : listasProbasCodigos) {
//            if (!validarProbabilidades(probas))
//                return mensajeProbabilidadesInvalidas();
//        }
//        for (ArrayList<String> lista : listasAlfabetos) {
//            if (!validarListasStringNoNulas(lista)) {
//                return mensajeListasConNulos("palabras del alfabeto fuente");
//            }
//        }
//        for (ArrayList<String> lista : listasPalabras) {
//            if (!validarListasStringNoNulas(lista)) {
//                return mensajeListasConNulos("palabras codigos");
//            }
//        }
//        // ArrayList<Double> rendimientos = new ArrayList<Double>();
//        HashMap<Integer, Double> rendimientos = new HashMap<Integer, Double>();
//
//        // Las listas deben ser de la misma longitud porque debe haber la misma cantidad de c�digos
//        // bloques.
//        if ((listasAlfabetos.size() != listasPalabras.size())
//                || (listasAlfabetos.size() != listasProbasCodigos.size())
//                || (listasPalabras.size() != listasProbasCodigos.size())) {
//            return new String(
//                    "Falta informacion. La cantidad de palabras y codigos deben ser iguales.");
//        }
//
//        for (int numeroCodigo = 0; numeroCodigo < listasAlfabetos.size(); numeroCodigo++) {
//            ArrayList<String> alfabetosCodigoActual = listasAlfabetos.get(numeroCodigo);
//            ArrayList<String> palabrasCodigoActual = listasPalabras.get(numeroCodigo);
//            ArrayList<Double> probabilidadesCodigoActual = listasProbasCodigos.get(numeroCodigo);
//
//            // Dentro de un c�digo bloque, la cantidad de s�mbolos, c�digos y probabilidades debe
//            // ser coincidente.
//            if ((alfabetosCodigoActual.size() != palabrasCodigoActual.size())
//                    || (alfabetosCodigoActual.size() != probabilidadesCodigoActual.size())
//                    || (palabrasCodigoActual.size() != probabilidadesCodigoActual.size())) {
//
//                return new String("Cantidad de elementos dispares dentro de codigo bloque"
//                        + Integer.toString(numeroCodigo + 1) + ".");
//
//                // De ser coincidente:
//            } else {
//                CodigoBloque unCodigoBloque = crearCodigo(probabilidadesCodigoActual,
//                        alfabetosCodigoActual, palabrasCodigoActual);
//
//                // @TODO: �base? => no importa... la elegis vos
//                rendimientos
//                        .put(numeroCodigo, (double) (unCodigoBloque
//                                .devolverEntropia(unidadPorDefecto) / unCodigoBloque
//                                .devolverLongitudMedia()));
//            }
//        }
//
//        // Double maximo = rendimientos.stream().max(Comparator.comparing(i -> i)).get();
//        Double maximo = (Collections.max(rendimientos.values()));
//        int numeroCodigoBloqueConMaximo = 0;
//
//        for (Entry<Integer, Double> entry : rendimientos.entrySet()) {
//            if (entry.getValue() == maximo) {
//                numeroCodigoBloqueConMaximo = entry.getKey();
//            }
//        }
//
//        String resultado = new String("El c�digo bloque m�s eficiente es el n�mero "
//                + Integer.toString(numeroCodigoBloqueConMaximo + 1) + " con un rendimiento de "
//                + maximo + ".");
//
//        return resultado;
//
//    }

    @Override
    public String determinarLongitudMedia(List<String> palabrasCodigos, List<Double> probabilidades) {
        CodigoBloque unCodigoBloque = new CodigoBloque();
        if (palabrasCodigos.size() != probabilidades.size())
            return "Falta informacion, la cantidad de palabras en el alfabeto debe ser igual a la cantidad de probabilidades que se asignan.";
        if (!validarProbabilidades(probabilidades))
            return mensajeProbabilidadesInvalidas();
        if (!validarListasStringNoNulas(palabrasCodigos)) {
            return mensajeListasConNulos("palabras codigos");
        }
        for (int numeroElemento = 0; numeroElemento < palabrasCodigos.size(); numeroElemento++) {
            unCodigoBloque.agregarUnidad(null, palabrasCodigos.get(numeroElemento), 
                    probabilidades.get(numeroElemento));
        }
        double longitud = unCodigoBloque.devolverLongitudMedia();
        String respuesta = "Las longitudes por palabra codigo son: \n";
        Map<String, Integer> longitudes = unCodigoBloque.devolverLongitudesDePalabraCodigo();
        for (String palabraCodigo : longitudes.keySet()) {
            respuesta += "  "+palabraCodigo+" -> "+longitudes.get(palabraCodigo)+"\n";
        }
        respuesta += "\n La longitud media del codigo es " + longitud + ".";
        return respuesta;
    }

    private CodigoBloque crearCodigo(List<Double> listaProbabilidades,
            List<String> listaAlfabetoFuente, List<String> listaPalabrasCodigo) {
        CodigoBloque unCodigoBloque = new CodigoBloque();
        for (int numeroElemento = 0; numeroElemento < listaAlfabetoFuente.size(); numeroElemento++) {
            unCodigoBloque.agregarUnidad(listaAlfabetoFuente.get(numeroElemento),
                    listaPalabrasCodigo.get(numeroElemento),
                    listaProbabilidades.get(numeroElemento));
        }
        return unCodigoBloque;
    }

    @Override
    public String propiedadesCodigoBloque(List<Double> listaProbabilidades,
            List<String> listaAlfabetoFuente, List<String> listaPalabrasCodigo, List<String> listaAlfabetoCodigo) {
        if (!validarProbabilidades(listaProbabilidades))
            return mensajeProbabilidadesInvalidas();
        if (!validarListasStringNoNulas(listaPalabrasCodigo)) {
            return mensajeListasConNulos("palabras codigos");
        }
        if (!validarListasStringNoNulas(listaAlfabetoFuente)) {
            return mensajeListasConNulos("palabras del alfabeto fuente");
        }
        if (!validarListasStringNoNulas(listaAlfabetoCodigo)) {
            return mensajeListasConNulos("palabras del alfabeto codigo");
        }
        CodigoBloque unCodigoBloque = crearCodigo(listaProbabilidades, listaAlfabetoFuente,
                listaPalabrasCodigo);
        unCodigoBloque.agregarAlfabetoCodigo(listaAlfabetoCodigo);
        String propiedades = "";
        if (unCodigoBloque.esSigular()) {
            propiedades += "- Es singular.\n";
        } else {
            propiedades += "- No es singular.\n";
        }
        if (unCodigoBloque.esUnivocamenteDecodificable()) {
            propiedades += "- Es univocamente decodificable.\n";
        } else {
            propiedades += "- No es univocamente decodificable.\n";
        }
        if (unCodigoBloque.esInstantaneo()) {
            propiedades += "- Es instantaneo.\n";
        } else {
            propiedades += "- No es instantaneo.\n";
        }
        return "Propiedades: \n" + propiedades;
    }

    @Override
    public String devolverCodigoInstantaneo(List<String> alfabetoFuente,
            List<String> alfabetoCodigos, List<Integer> longitudes,  List<Double> probabilidades) {
        if (!validarListasStringNoNulas(alfabetoCodigos)) {
            return mensajeListasConNulos("alfabeto codigo");
        }
        if (!validarListasStringNoNulas(alfabetoFuente)) {
            return mensajeListasConNulos("palabras del alfabeto fuente");
        }
        if (alfabetoCodigos.size() <= 1) {
            return "Debe existir mas de un simbolo en el alfabeto codigo.";
        }
        ObtenedorCodigoInstantaneo obtenedor = new ObtenedorCodigoInstantaneo();
        if (!validarProbabilidades(probabilidades))
            return mensajeProbabilidadesInvalidas();
        CodigoBloque codigo = obtenedor.armarCodigo(longitudes, alfabetoFuente, alfabetoCodigos, probabilidades);
        if (codigo == null)
            return "El codigo no cumple Kraft, no existe un codigo instantaneo con esas longitudes.";
        return imprimirPalabraFuenteYPalabraCodigo(codigo);
    }

    /**
     * Devuelve el mensaje que corresponda al comparar entropias
     * 
     * @param entropiaDelCodigoDelUsuario : valor de la entropia que no sabemos si es equiprobable.
     * @param entropiaEquiprobable : valor de la entropia que sabemos que es equiprobable.
     * @param unidad : base en la que se saco las entropias.
     * @return mensaje de la comparacion.
     */
    private String obtenerSignificadoCompararEntropias(double entropiaDelCodigoDelUsuario,
            double entropiaEquiprobable, String unidad) {
        if (entropiaDelCodigoDelUsuario < entropiaEquiprobable)
            return "La entropia de la distribucion de probabilidades ingresada es: "
                    + entropiaDelCodigoDelUsuario
                    + " esta entropia es menor a la entropia de la distribucion de probabilidades equiprobables: "
                    + entropiaEquiprobable ;
        if (entropiaDelCodigoDelUsuario == entropiaEquiprobable)
            return "La entropia de la distribucion de probabilidades ingresada es: "
                    + entropiaDelCodigoDelUsuario
                    + " esta entropia es igual a la entropia de la distribucion de probabilidades equiprobables: "
                    + entropiaEquiprobable ;
        return "La entropia de la distribucion de probabilidades ingresada es: "
                + entropiaDelCodigoDelUsuario
                + " esta entropia es mayor a la entropia de la distribucion de probabilidades equiprobables: "
                + entropiaEquiprobable ;

    }

    /**
     * Devuelve un String con las palabras fuentes con su respectiva palabra codigo.
     */
    private String imprimirPalabraFuenteYPalabraCodigo(CodigoBloque codigo) {
        String resultado = "(Palabra fuente - Palabra codigo) \n";
        List<String> palabrasFuentes = codigo.devolverAlfabetoFuente();
        List<String> palabrasCodigos = codigo.devolverPalabrasCodigos();
        for (int i = 0; i < palabrasFuentes.size(); i++) {
            resultado += "( " + palabrasFuentes.get(i) + " - " + palabrasCodigos.get(i) + " ) \n";
        }
        return resultado;
    }

}
