package interfazGrafica.ventanasEntropia;

import java.util.List;

import javax.swing.JTextPane;

public interface Calculable {
	void calcular(List<Double> listaProbabilidades, String unidad, JTextPane resultadoEntropia);
}
