package interfazGrafica.ventanaCodigoCompleto;

import interfazGrafica.ventanaError.VentanaError;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import controlador.Menu;
import controlador.MenuImplementacion;

public class VentanaCodigoPropiedades {
    private JFrame frame;
    private List<Double> listaProbabilidades;
    private List<String> listaPalabrasCodigo;
    private List<String> listaAlfabetoFuente;
    private List<String> listaAlfabetoCodigo;
    private String listaCodigo;
    private String listaImpAlfabetoCodigo;
    private JTextField textAlfabeto;
    private JTextField textProbabilidad;
    private JTextField textPalabraCodigo;
    private JTextField simboloAlfabetoCodigo;
    private JTextArea textCodigoIngresado;

    /**
     * Create the application.
     */
    public VentanaCodigoPropiedades() {
        this.listaAlfabetoFuente = new ArrayList<String>();
        this.listaPalabrasCodigo = new ArrayList<String>();
        this.listaProbabilidades = new ArrayList<Double>();
        listaAlfabetoCodigo = new ArrayList<String>();
        listaCodigo = "(vacio)";
        listaImpAlfabetoCodigo = "";
        initialize();
        /** Para abrir */
        frame.setVisible(true);
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 742, 451);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel lblCodigoBloque = new JLabel("Propiedades");
        lblCodigoBloque.setFont(new Font("Tahoma", Font.BOLD, 20));
        lblCodigoBloque.setHorizontalAlignment(SwingConstants.CENTER);
        
        JLabel lblSimboloDelAlfabeto = new JLabel("Simbolo del alfabeto:");
        
        textAlfabeto = new JTextField();
        textAlfabeto.setColumns(10);
        
        JLabel lblProbabilidad = new JLabel("Probabilidad:");
        
        textProbabilidad = new JTextField();
        textProbabilidad.setColumns(10);
        
        JLabel lblPalabraCodigo = new JLabel("Palabra codigo:");
        
        textPalabraCodigo = new JTextField();
        textPalabraCodigo.setColumns(10);
        
        JScrollPane scrollPane = new JScrollPane();
        
        JScrollPane scrollPane_1 = new JScrollPane();
        
        JTextArea textResultado = new JTextArea();
        scrollPane_1.setViewportView(textResultado);
        
        textCodigoIngresado = new JTextArea();
        textCodigoIngresado.setText("(vacio)");
        scrollPane.setViewportView(textCodigoIngresado);
        
        JButton btnCalcularPropiedadesDel = new JButton("Calcular propiedades del codigo");
        btnCalcularPropiedadesDel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Menu funciones = new MenuImplementacion();
                String resultado = funciones.propiedadesCodigoBloque(listaProbabilidades, listaAlfabetoFuente, listaPalabrasCodigo, listaAlfabetoCodigo);
                textResultado.setText(resultado);
            }
        });
        
        JButton btnAgregarSimbolo = new JButton("Agregar simbolo");
        btnAgregarSimbolo.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String palabraFuente = textAlfabeto.getText();
                if ( estaVacio(palabraFuente) ){
                    new VentanaError("Palabra fuente no debe ser vacia.");
                    return;
                }
                if (listaAlfabetoFuente.contains(palabraFuente)) {
                    new VentanaError("La palabra del alfabeto fuente ya fue ingresado, intente con otra palabra.");
                    return;
                }
                String palabraCodigo = textPalabraCodigo.getText();
                if ( estaVacio(palabraCodigo) ){
                    new VentanaError("Palabra codigo no debe ser vacia.");
                    return;
                }
                if (listaPalabrasCodigo.contains(palabraCodigo)) {
                    new VentanaError("La palabra codigo ya fue ingresado, intente con otra palabra.");
                    return;
                }
                if ( !caracteresStringPertenecenALista(palabraCodigo, listaAlfabetoCodigo) ) {
                    new VentanaError("La palabra codigo ingresada tiene uno o mas simbolos que no se encuentran en el alfabeto codigo.");
                    return;
                }
                NumberFormat format = NumberFormat.getInstance(Locale.US);
                try {
                    Number number = format.parse(textProbabilidad.getText());
                    double numberDouble = number.doubleValue();
                    if (numberDouble <= 0) {
                        new VentanaError("La probabilidad debe ser mayor a 0.");
                        return;
                    }
                    if (numberDouble >= 1) {
                        new VentanaError("La probabilidad debe ser menor a 1.");
                        return;
                    }
                    listaAlfabetoFuente.add(palabraFuente);
                    listaPalabrasCodigo.add(palabraCodigo);
                    listaProbabilidades.add(numberDouble);
                    if (listaCodigo.contentEquals("(vacio)"))
                        listaCodigo = "( Palabra fuente ; probabilidad ; palabra codigo )";
                    listaCodigo += "( " + textAlfabeto.getText()+" ; " + textProbabilidad.getText() + " ; " + textPalabraCodigo.getText() + " )\n";
                    //textCodigoIngresado.setText(listaImpAlfabetoCodigo);
//                    textCodigoIngresado.setText(listaCodigo);
                    imprimirDatos();
                } catch (ParseException exception) {
                    new VentanaError("La probabilidad ingresada no es un valor numerico o no se ingreso una probabilidad.");
                    return;
                }
            }
        });
        
        JLabel lblCodigo = new JLabel("Codigo:");
        
        JLabel lblPropiedades = new JLabel("Propiedades:");
        
        JButton btnVolver = new JButton("Volver");
        btnVolver.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                frame.setVisible(false);
            }
        });
        
        simboloAlfabetoCodigo = new JTextField();
        simboloAlfabetoCodigo.setColumns(10);
        
        JLabel lblNewLabel = new JLabel("Simbolo del alfabeto codigo:");
        
        JButton btnAgregarSimboloDel = new JButton("Agregar simbolo del alfabeto codigo");
        btnAgregarSimboloDel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String simbolo = simboloAlfabetoCodigo.getText();
                if (estaVacio(simbolo)) {
                    new VentanaError("El simbolo del alfabeto codigo no debe ser vacio.");
                    return;
                }
                if (simbolo.length() != 1) {
                    new VentanaError("El simbolo del alfabeto codigo debe solo contener un caracter.");
                    return;
                }
                if (listaAlfabetoCodigo.contains(simbolo)) {
                    new VentanaError("El simbolo del alfabeto ya fue ingresado, intente con otro simbolo.");
                    return;
                }
                listaAlfabetoCodigo.add(simbolo);
                if (listaImpAlfabetoCodigo.equals("")) {
                    listaImpAlfabetoCodigo += "Alfabeto codigo:\n";
                }
                listaImpAlfabetoCodigo += " " + simboloAlfabetoCodigo.getText() + "\n";
                imprimirDatos();
            }
        });
        GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
        groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.TRAILING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addGap(317)
                    .addComponent(btnVolver, GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
                    .addGap(295))
                .addGroup(groupLayout.createSequentialGroup()
                    .addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
                        .addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
                            .addGap(24)
                            .addComponent(lblSimboloDelAlfabeto)
                            .addPreferredGap(ComponentPlacement.UNRELATED)
                            .addComponent(textAlfabeto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(ComponentPlacement.RELATED, 102, Short.MAX_VALUE)
                            .addComponent(lblProbabilidad))
                        .addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
                            .addGap(28)
                            .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                .addComponent(scrollPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
                                .addComponent(btnAgregarSimbolo, GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
                                .addGroup(groupLayout.createSequentialGroup()
                                    .addComponent(lblCodigo)
                                    .addPreferredGap(ComponentPlacement.RELATED))))
                        .addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(lblCodigoBloque, GroupLayout.PREFERRED_SIZE, 234, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblNewLabel)))
                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                            .addGap(18)
                            .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                .addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)
                                .addComponent(btnCalcularPropiedadesDel, GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)
                                .addComponent(lblPropiedades)))
                        .addGroup(groupLayout.createSequentialGroup()
                            .addGap(4)
                            .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                .addGroup(groupLayout.createSequentialGroup()
                                    .addComponent(simboloAlfabetoCodigo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(ComponentPlacement.RELATED)
                                    .addComponent(btnAgregarSimboloDel))
                                .addGroup(groupLayout.createSequentialGroup()
                                    .addComponent(textProbabilidad, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblPalabraCodigo)
                                    .addGap(18)
                                    .addComponent(textPalabraCodigo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                            .addPreferredGap(ComponentPlacement.RELATED)))
                    .addGap(43))
        );
        groupLayout.setVerticalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addGap(18)
                    .addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
                        .addGroup(groupLayout.createSequentialGroup()
                            .addComponent(lblCodigoBloque, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                            .addGap(18))
                        .addGroup(groupLayout.createSequentialGroup()
                            .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                .addComponent(simboloAlfabetoCodigo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnAgregarSimboloDel)
                                .addComponent(lblNewLabel))
                            .addPreferredGap(ComponentPlacement.UNRELATED)))
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(textProbabilidad, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblProbabilidad)
                        .addComponent(lblSimboloDelAlfabeto)
                        .addComponent(textAlfabeto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(textPalabraCodigo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblPalabraCodigo))
                    .addPreferredGap(ComponentPlacement.UNRELATED)
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(btnAgregarSimbolo)
                        .addComponent(btnCalcularPropiedadesDel))
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                        .addComponent(lblCodigo)
                        .addComponent(lblPropiedades))
                    .addPreferredGap(ComponentPlacement.RELATED)
                    .addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
                        .addComponent(scrollPane_1)
                        .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE))
                    .addPreferredGap(ComponentPlacement.UNRELATED, 20, Short.MAX_VALUE)
                    .addComponent(btnVolver)
                    .addContainerGap())
        );
        frame.getContentPane().setLayout(groupLayout);
    }

    private void imprimirDatos() {
        String datos = "";
        datos += listaImpAlfabetoCodigo;
        if (listaCodigo.equals("(vacio)"))
            listaCodigo = "Codigo ingresado: \n";
        datos += listaCodigo;
        System.out.println("Alfabet: " + simboloAlfabetoCodigo.getText());
        textCodigoIngresado.setText(datos);
    }

    private boolean estaVacio(String palabra) {
        if (palabra == null) {
            return true;
        }

        return palabra.trim().isEmpty();
    }

    private boolean caracteresStringPertenecenALista(String str, List<String> list) {
        if (list.size() == 0) {
            return false;
        }
        String[] chars = str.split("");
        for (String sch : chars) {
            if (list.contains(sch) == false) {
                return false;
            }
        }

        return true;
    }
    
}

// public class VentanaCodigoPropiedades {
//
// private JFrame frame;
// private List<Double> listaProbabilidades;
// private List<String> listaPalabrasCodigo;
// private List<String> listaAlfabetoFuente;
// private List<String> listaAlfabetoCodigo;
// private String listaCodigo;
// private JTextField textAlfabeto;
// private JTextField textProbabilidad;
// private JTextField textField;
//
// /**
// * Create the application.
// */
// public VentanaCodigoPropiedades() {
// this.listaAlfabetoFuente= new ArrayList<String>();
// this.listaPalabrasCodigo= new ArrayList<String>();
// this.listaProbabilidades= new ArrayList<Double>();
// listaCodigo="(vacio)";
// initialize();
// /** Para abrir */
// frame.setVisible(true);
// }
//
// /**
// * Initialize the contents of the frame.
// */
// private void initialize() {
// frame = new JFrame();
// frame.setBounds(100, 100, 691, 437);
// frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//
// JLabel lblCodigoBloque = new JLabel("Codigo Bloque");
// lblCodigoBloque.setFont(new Font("Tahoma", Font.BOLD, 20));
// lblCodigoBloque.setHorizontalAlignment(SwingConstants.CENTER);
//
// JLabel lblSimboloDelAlfabeto = new JLabel("Simbolo del alfabeto:");
//
// textAlfabeto = new JTextField();
// textAlfabeto.setColumns(10);
//
// JLabel lblProbabilidad = new JLabel("Probabilidad:");
//
// textProbabilidad = new JTextField();
// textProbabilidad.setColumns(10);
//
// JLabel lblPalabraCodigo = new JLabel("Palabra codigo:");
//
// textField = new JTextField();
// textField.setColumns(10);
//
// JScrollPane scrollPane = new JScrollPane();
//
// JScrollPane scrollPane_1 = new JScrollPane();
//
// JTextArea textResultado = new JTextArea();
// scrollPane_1.setViewportView(textResultado);
//
// JTextArea textCodigoIngresado = new JTextArea();
// textCodigoIngresado.setText("(vacio)");
// scrollPane.setViewportView(textCodigoIngresado);
//
// JButton btnCalcularPropiedadesDel = new JButton("Calcular propiedades del codigo");
// btnCalcularPropiedadesDel.addMouseListener(new MouseAdapter() {
// @Override
// public void mouseClicked(MouseEvent e) {
// Menu funciones = new MenuImplementacion();
// //FALTA AGREGAR LA LISTA DEL ALFABETO CODIGO!!! TODO
// String resultado = funciones.propiedadesCodigoBloque(listaProbabilidades, listaAlfabetoFuente,
// listaPalabrasCodigo, listaAlfabetoCodigo);
// textResultado.setText(resultado);
// }
// });
//
// JButton btnAgregarSimbolo = new JButton("Agregar simbolo");
// btnAgregarSimbolo.addMouseListener(new MouseAdapter() {
// @Override
// public void mouseClicked(MouseEvent e) {
// listaAlfabetoFuente.add(textAlfabeto.getText());
// listaPalabrasCodigo.add(textField.getText());
// NumberFormat format = NumberFormat.getInstance(Locale.US);
// try {
// Number number = format.parse(textProbabilidad.getText());
// double numberDouble = number.doubleValue();
// listaProbabilidades.add(numberDouble);
// if (listaCodigo.contentEquals("(vacio)"))
// listaCodigo = "";
// listaCodigo += "( " + textAlfabeto.getText()+" ; " + textProbabilidad.getText() + " ; " +
// textField.getText() + " )\n";
// textCodigoIngresado.setText(listaCodigo);
// } catch (ParseException exception) {
// }
// }
// });
//
// JLabel lblCodigo = new JLabel("Codigo:");
//
// JLabel lblPropiedades = new JLabel("Propiedades:");
//
// JButton btnVolver = new JButton("Volver");
// btnVolver.addMouseListener(new MouseAdapter() {
// @Override
// public void mouseClicked(MouseEvent e) {
// frame.setVisible(false);
// }
// });
// GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
// groupLayout.setHorizontalGroup(
// groupLayout.createParallelGroup(Alignment.TRAILING)
// .addGroup(groupLayout.createSequentialGroup()
// .addContainerGap()
// .addComponent(lblCodigoBloque, GroupLayout.DEFAULT_SIZE, 646, Short.MAX_VALUE)
// .addGap(19))
// .addGroup(groupLayout.createSequentialGroup()
// .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
// .addGroup(groupLayout.createSequentialGroup()
// .addGap(24)
// .addComponent(lblSimboloDelAlfabeto)
// .addPreferredGap(ComponentPlacement.UNRELATED)
// .addComponent(textAlfabeto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
// GroupLayout.PREFERRED_SIZE)
// .addPreferredGap(ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
// .addComponent(lblProbabilidad))
// .addGroup(groupLayout.createSequentialGroup()
// .addGap(28)
// .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
// .addComponent(scrollPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE)
// .addComponent(btnAgregarSimbolo, GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE)
// .addGroup(groupLayout.createSequentialGroup()
// .addComponent(lblCodigo)
// .addPreferredGap(ComponentPlacement.RELATED)))))
// .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
// .addGroup(groupLayout.createSequentialGroup()
// .addGap(4)
// .addComponent(textProbabilidad, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
// GroupLayout.PREFERRED_SIZE)
// .addPreferredGap(ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
// .addComponent(lblPalabraCodigo)
// .addGap(18)
// .addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
// GroupLayout.PREFERRED_SIZE)
// .addGap(15))
// .addGroup(groupLayout.createSequentialGroup()
// .addGap(18)
// .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
// .addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
// .addComponent(btnCalcularPropiedadesDel, GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
// .addComponent(lblPropiedades))))
// .addGap(13))
// .addGroup(groupLayout.createSequentialGroup()
// .addGap(317)
// .addComponent(btnVolver, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
// .addGap(295))
// );
// groupLayout.setVerticalGroup(
// groupLayout.createParallelGroup(Alignment.LEADING)
// .addGroup(groupLayout.createSequentialGroup()
// .addGap(18)
// .addComponent(lblCodigoBloque, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
// .addGap(18)
// .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
// .addComponent(textProbabilidad, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
// GroupLayout.PREFERRED_SIZE)
// .addComponent(lblProbabilidad)
// .addComponent(lblSimboloDelAlfabeto)
// .addComponent(textAlfabeto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
// GroupLayout.PREFERRED_SIZE)
// .addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
// GroupLayout.PREFERRED_SIZE)
// .addComponent(lblPalabraCodigo))
// .addPreferredGap(ComponentPlacement.UNRELATED)
// .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
// .addComponent(btnAgregarSimbolo)
// .addComponent(btnCalcularPropiedadesDel))
// .addPreferredGap(ComponentPlacement.RELATED)
// .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
// .addComponent(lblCodigo)
// .addComponent(lblPropiedades))
// .addPreferredGap(ComponentPlacement.RELATED)
// .addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
// .addComponent(scrollPane_1)
// .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE))
// .addPreferredGap(ComponentPlacement.UNRELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
// .addComponent(btnVolver)
// .addContainerGap())
// );
// frame.getContentPane().setLayout(groupLayout);
// }
// }