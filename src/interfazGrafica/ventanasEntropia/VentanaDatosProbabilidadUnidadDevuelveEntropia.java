package interfazGrafica.ventanasEntropia;

import interfazGrafica.ventanaError.VentanaError;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.SpringLayout;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import java.awt.Font;
import javax.swing.SwingConstants;

public class VentanaDatosProbabilidadUnidadDevuelveEntropia {

	private JFrame frame;
	private JTextField txtProbabilidad;
	private List<Double> listaProbabilidades;
	private String unidad;
	private String listaProbabilidadesTexto;
	private Calculable calculador;
	
	/**
	 * Create the application.
	 */
	public VentanaDatosProbabilidadUnidadDevuelveEntropia(Calculable calculador) {
		this.calculador = calculador;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		listaProbabilidades = new ArrayList<Double>();
		unidad = "";
		listaProbabilidadesTexto = "(vacio)";
		frame = new JFrame();
		frame.addMouseListener(new MouseAdapter() {
	
		});
		frame.setBounds(100, 100, 523, 395);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);

		JLabel lblIngreseLaSiguiente = new JLabel("Calculo de la Entropia");
		lblIngreseLaSiguiente.setHorizontalAlignment(SwingConstants.CENTER);
		springLayout.putConstraint(SpringLayout.WEST, lblIngreseLaSiguiente, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblIngreseLaSiguiente, -10, SpringLayout.EAST, frame.getContentPane());
		lblIngreseLaSiguiente.setFont(new Font("Tahoma", Font.BOLD, 17));
		springLayout.putConstraint(SpringLayout.NORTH, lblIngreseLaSiguiente, 10, SpringLayout.NORTH, frame.getContentPane());
		frame.getContentPane().add(lblIngreseLaSiguiente);

		JLabel lblNewLabel = new JLabel("Probabilidad");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel, 49, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel, 36, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblNewLabel);

		txtProbabilidad = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, txtProbabilidad, -3, SpringLayout.NORTH, lblNewLabel);
		springLayout.putConstraint(SpringLayout.WEST, txtProbabilidad, 17, SpringLayout.EAST, lblNewLabel);
		springLayout.putConstraint(SpringLayout.EAST, txtProbabilidad, -327, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(txtProbabilidad);
		txtProbabilidad.setColumns(10);

		JTextPane textListaProbabilidades = new JTextPane();
		springLayout.putConstraint(SpringLayout.SOUTH, textListaProbabilidades, -56, SpringLayout.SOUTH, frame.getContentPane());
		textListaProbabilidades.setText(this.listaProbabilidadesTexto);
		springLayout.putConstraint(SpringLayout.NORTH, textListaProbabilidades, 81, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, textListaProbabilidades, 36, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, textListaProbabilidades, 207, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(textListaProbabilidades);

		JButton btnAgregar = new JButton("Agregar");
		springLayout.putConstraint(SpringLayout.SOUTH, btnAgregar, 17, SpringLayout.NORTH, lblNewLabel);
		springLayout.putConstraint(SpringLayout.EAST, btnAgregar, 112, SpringLayout.EAST, txtProbabilidad);
		btnAgregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { // Agrega las probabilidades a la lista de probabilidades
				NumberFormat format = NumberFormat.getInstance(Locale.US);
				try {
					Number number = format.parse(txtProbabilidad.getText());
					double numberDouble = number.doubleValue();
					if (numberDouble <= 0) {
                        new VentanaError("La probabilidad debe ser mayor a 0.");
                        return;
                    }
                    if (numberDouble >= 1) {
                        new VentanaError("La probabilidad debe ser menor a 1.");
                        return;
                    }
					listaProbabilidades.add(numberDouble);
					if (listaProbabilidadesTexto.contentEquals("(vacio)"))
						listaProbabilidadesTexto = "";
					listaProbabilidadesTexto += txtProbabilidad.getText() + "\n";
					textListaProbabilidades.setText(listaProbabilidadesTexto);
				} catch (ParseException exception) {
				    new VentanaError("La probabilidad debe ser un valor numerico entre 0 y 1.");
                    return;
				}
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, btnAgregar, -4, SpringLayout.NORTH, lblNewLabel);
		springLayout.putConstraint(SpringLayout.WEST, btnAgregar, 6, SpringLayout.EAST, txtProbabilidad);
		frame.getContentPane().add(btnAgregar);

		JComboBox comboBoxUnidades = new JComboBox();
		springLayout.putConstraint(SpringLayout.NORTH, comboBoxUnidades, 46, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, comboBoxUnidades, -151, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, comboBoxUnidades, -35, SpringLayout.EAST, frame.getContentPane());
		comboBoxUnidades.setModel(new DefaultComboBoxModel(new String[] { "bits", "base 3", "base 4", "base 5", "base 6", "base 7",
				"base 8", "base e" }));
		frame.getContentPane().add(comboBoxUnidades);

		JLabel lblUnidades = new JLabel("Unidades");
		springLayout.putConstraint(SpringLayout.NORTH, lblUnidades, 0, SpringLayout.NORTH, lblNewLabel);
		springLayout.putConstraint(SpringLayout.EAST, lblUnidades, -6, SpringLayout.WEST, comboBoxUnidades);
		frame.getContentPane().add(lblUnidades);

		JLabel lblLaEntropiaEs = new JLabel("Resultado:");
		frame.getContentPane().add(lblLaEntropiaEs);

		JTextPane resultadoEntropia = new JTextPane();
		springLayout.putConstraint(SpringLayout.NORTH, resultadoEntropia, 6, SpringLayout.SOUTH, lblLaEntropiaEs);
		springLayout.putConstraint(SpringLayout.WEST, resultadoEntropia, 33, SpringLayout.EAST, textListaProbabilidades);
		springLayout.putConstraint(SpringLayout.SOUTH, resultadoEntropia, 0, SpringLayout.SOUTH, textListaProbabilidades);
		springLayout.putConstraint(SpringLayout.EAST, resultadoEntropia, -35, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(resultadoEntropia);

		JButton btnCalcular = new JButton("Calcular");
		springLayout.putConstraint(SpringLayout.WEST, btnCalcular, 33, SpringLayout.EAST, textListaProbabilidades);
		springLayout.putConstraint(SpringLayout.EAST, btnCalcular, -35, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, lblLaEntropiaEs, 6, SpringLayout.SOUTH, btnCalcular);
		springLayout.putConstraint(SpringLayout.WEST, lblLaEntropiaEs, 0, SpringLayout.WEST, btnCalcular);
		springLayout.putConstraint(SpringLayout.NORTH, btnCalcular, 0, SpringLayout.NORTH, textListaProbabilidades);
		btnCalcular.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/** Aca se calcula el valor de la entropia */
				unidad = (String) comboBoxUnidades.getSelectedItem();
				calculador.calcular(listaProbabilidades, unidad, resultadoEntropia);
			}
		});
		frame.getContentPane().add(btnCalcular);
		
		JButton btnVolverAlMenu = new JButton("Volver");
		springLayout.putConstraint(SpringLayout.WEST, btnVolverAlMenu, 209, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, btnVolverAlMenu, -10, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnVolverAlMenu, -197, SpringLayout.EAST, frame.getContentPane());
		btnVolverAlMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/** Para cerrar TODO */
				frame.setVisible(false);
			}
		});
		frame.getContentPane().add(btnVolverAlMenu);
		/** Para abrir TODO */
		frame.setVisible(true);
	}
}
