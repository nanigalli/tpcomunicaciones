package interfazGrafica.ventanaError;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class VentanaError {

    private JFrame frame;

    /**
     * Create the application.
     */
    public VentanaError(String mensaje) {
        initialize(mensaje);
        /** Para abrir TODO */
        frame.setVisible(true);
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize(String mensaje) {
        frame = new JFrame();
        frame.setBounds(100, 100, 452, 297);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel label = new JLabel("Error");
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setFont(new Font("Tahoma", Font.PLAIN, 25));
        
        JLabel mensajeError = new JLabel(mensaje);
        mensajeError.setFont(new Font("Tahoma", Font.PLAIN, 13));
        mensajeError.setHorizontalAlignment(SwingConstants.CENTER);
        
        JButton botonError = new JButton("Volver");
        botonError.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                /** Para cierro la ventana TODO */
                frame.setVisible(false);
            }
        });
        GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
        groupLayout.setHorizontalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addComponent(label, GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
                    .addGap(2))
                .addGroup(groupLayout.createSequentialGroup()
                    .addGap(133)
                    .addComponent(botonError, GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE)
                    .addGap(128))
                .addGroup(groupLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(mensajeError, GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE)
                    .addGap(12))
        );
        groupLayout.setVerticalGroup(
            groupLayout.createParallelGroup(Alignment.LEADING)
                .addGroup(groupLayout.createSequentialGroup()
                    .addGap(31)
                    .addComponent(label)
                    .addGap(21)
                    .addComponent(mensajeError, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, Short.MAX_VALUE)
                    .addComponent(botonError, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                    .addGap(42))
        );
        frame.getContentPane().setLayout(groupLayout);
    }
}
