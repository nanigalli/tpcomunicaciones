package elementosCodigo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CodigoBloque {
	private List<UnidadCodigoBloque> codigo;
	private List<String> AlfabetoCodigo;

	public CodigoBloque() {
		codigo = new ArrayList<UnidadCodigoBloque>();
	}
	
	public void agregarAlfabetoCodigo(List<String> alfabeto) {
	    AlfabetoCodigo = new ArrayList<String>();
	    for (String simbolo : alfabeto)
	        this.AlfabetoCodigo.add(simbolo);
	}
	
	public List<String> devolverAlfabetoFuente() {
	    List<String> resultado = new ArrayList<String>();
	    for (UnidadCodigoBloque unidad : codigo) {
	        resultado.add(unidad.devolverPalabraFuente());
	    }
	    return resultado;
	}
	
   public List<String> devolverPalabrasCodigos() {
        List<String> resultado = new ArrayList<String>();
        for (UnidadCodigoBloque unidad : codigo) {
            resultado.add(unidad.devolverPalabraCodigo());
        }
        return resultado;
    }

	public void agregarUnidad(String simboloFuente, String palabraCodigo, double probabilidad) {
		UnidadCodigoBloque unidad = new UnidadCodigoBloque(simboloFuente, palabraCodigo, probabilidad);
		this.codigo.add(unidad);
	}

	
	public boolean esSigular() {
		Iterator<UnidadCodigoBloque> iterador1 = codigo.iterator();
		int posicion1 = 0;
		while (iterador1.hasNext()) {
			UnidadCodigoBloque unidad1 = iterador1.next();
			posicion1++;
			int posicion2 = posicion1;
			while (posicion2 < codigo.size()) {
				UnidadCodigoBloque unidad2 = codigo.get(posicion2);
				if (unidad1.sonIgualesLasPalabrasCodigos(unidad2))
					return true;
				posicion2++;
			}
		}
		return false;
	}
	
	/**
	 * Devuelve una copia del codigo bloque.
	 */
	private List<UnidadCodigoBloque> obtenerCopiaDelBloque() {
	    List<UnidadCodigoBloque> codigo2 = new ArrayList<UnidadCodigoBloque>();
	    for (UnidadCodigoBloque unidad : codigo) {
	        codigo2.add(unidad);
	    }
	    return codigo2;
	}
	
	/**
	 * Devuelve una lista con las longitudes de las palabras codigos del codigo.
	 */
	private List<Integer> obtenerListaDeLongitudes() {
	    List<Integer> lista = new ArrayList<Integer>();
	    for (UnidadCodigoBloque unidad : codigo) {
	        lista.add(unidad.devolverLongitudPalabraCodigo());
	    }
	    return lista;
	}
	
	public boolean esInstantaneo() {
	    if (esSigular() || !esUnivocamenteDecodificable())
            return false;
	    Kraft kraft = new Kraft();
	    return kraft.cumple(obtenerListaDeLongitudes(), AlfabetoCodigo.size());
	}

	public boolean esUnivocamenteDecodificable() {
		List<UnidadCodigoBloque> codigo1 = obtenerCopiaDelBloque();
		Iterator<UnidadCodigoBloque> iterador1 = codigo1.iterator();
		while (iterador1.hasNext()) {
			UnidadCodigoBloque unidad1 = iterador1.next();
			iterador1.remove();
			List<String> prefijos = unidad1.obtenerPrefijosPalabraCodigo();
			for (UnidadCodigoBloque unidad2 : codigo)
				for (String prefijo : prefijos)
					if (unidad2.sonIgualesLasPalabrasCodigos(prefijo)) {
						return false;
					}
		}
		return true;
	}

	public float devolverLongitudMedia() {
		float longitudMedia = 0;
		for (UnidadCodigoBloque unidad : codigo) {
			longitudMedia += unidad.devolverLongitudPalabraCodigo() * unidad.devolverProbabilidad();
		}
		return longitudMedia;
	}

	public float devolverEntropia(double base) {
		float entropia = 0;
		for (UnidadCodigoBloque unidad : codigo) {
			double probabilidadALaMenos1 = 1 / unidad.devolverProbabilidad();
			entropia += (Math.log(probabilidadALaMenos1) / Math.log(base)) / probabilidadALaMenos1;
		}
		return entropia;
	}
	
	/**
	 * Devuelve un mapa con las palabras codigos como Key y las longitudes de cada palabra como dato del mapa.
	 */
	public Map<String, Integer> devolverLongitudesDePalabraCodigo() {
	    Map<String, Integer> mapa = new HashMap<String, Integer>();
	    for (UnidadCodigoBloque unidad : codigo) {
            mapa.put(unidad.devolverPalabraCodigo(), unidad.devolverLongitudPalabraCodigo());
        }
	    return mapa;
	}
	
}
