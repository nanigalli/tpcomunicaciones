package interfazGrafica.ventanasEntropia;

import java.util.List;

import javax.swing.JTextPane;

import com.sun.javafx.scene.layout.region.Margins.Converter;

import controlador.Menu;
import controlador.MenuImplementacion;

public class ComparadorDeEntropia implements Calculable{

	@Override
	public void calcular(List<Double> listaProbabilidades, String unidad, JTextPane resultadoEntropia) {
		Menu funciones = new MenuImplementacion();
		String resultado = "";
		resultado += funciones.compararEntropiaConUnaDistribucionEquiprobable(listaProbabilidades, unidad);
		resultadoEntropia.setText(resultado);
	}

}
