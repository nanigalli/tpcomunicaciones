import elementosCodigo.CodigoBloque;
import elementosCodigo.ObtenedorCodigoInstantaneo;

import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ObtenedorCodigosInstantaneosTest {

    private static final double DELTA = 0.00001;

    @Test
    public void testObtenerCodigoInstantaneo() {
        ObtenedorCodigoInstantaneo obtenedor = new ObtenedorCodigoInstantaneo();
        List<Integer> tamanioLongitudes = new ArrayList<Integer>();
        tamanioLongitudes.add(1);
        tamanioLongitudes.add(3);
        tamanioLongitudes.add(3);
        tamanioLongitudes.add(3);
        List<Double> listaProba = new ArrayList<Double>();
        listaProba.add(0.25);
        listaProba.add(0.25);
        listaProba.add(0.4);
        listaProba.add(0.1);
        List<String> alfabetoFuente = new ArrayList<String>();
        // Cargo el alfabeto codigo
        alfabetoFuente.add("hola");
        alfabetoFuente.add("jamon");
        alfabetoFuente.add("lomo");
        alfabetoFuente.add("burra");
        List<String> alfabetoCodigo = new ArrayList<String>();
        // Cargo el alfabeto codigo
        alfabetoCodigo.add("0");
        alfabetoCodigo.add("1");
        // Obtengo el codigo
        CodigoBloque codigo = obtenedor.armarCodigo(tamanioLongitudes, alfabetoFuente,
                alfabetoCodigo, listaProba);
        List<String> palabrasFuentesCodigo = codigo.devolverAlfabetoFuente();
        List<String> palabrasCodigos = codigo.devolverPalabrasCodigos();
        assertEquals(palabrasFuentesCodigo.get(0), "lomo");
        assertEquals(palabrasCodigos.get(0), "0");
        assertEquals(palabrasFuentesCodigo.get(1), "hola");
        assertEquals(palabrasCodigos.get(1), "100");
        assertEquals(palabrasFuentesCodigo.get(2), "jamon");
        assertEquals(palabrasCodigos.get(2), "101");
        assertEquals(palabrasFuentesCodigo.get(3), "burra");
        assertEquals(palabrasCodigos.get(3), "110");
    }

    @Test
    public void testFallarPorKraft() {
        ObtenedorCodigoInstantaneo obtenedor = new ObtenedorCodigoInstantaneo();
        List<Integer> tamanioLongitudes = new ArrayList<Integer>();
        tamanioLongitudes.add(1);
        tamanioLongitudes.add(3);
        tamanioLongitudes.add(3);
        tamanioLongitudes.add(3);
        tamanioLongitudes.add(3);
        tamanioLongitudes.add(3);
        List<String> alfabetoFuente = new ArrayList<String>();
        // Cargo el alfabeto codigo
        alfabetoFuente.add("hola");
        alfabetoFuente.add("jamon");
        alfabetoFuente.add("lomo");
        alfabetoFuente.add("burra");
        alfabetoFuente.add("burra1");
        alfabetoFuente.add("burra2");
        List<Double> listaProba = new ArrayList<Double>();
        // Cargo de probabilidades
        listaProba.add(0.25);
        listaProba.add(0.25);
        listaProba.add(0.1);
        listaProba.add(0.1);
        listaProba.add(0.1);
        listaProba.add(0.2);
        List<String> alfabetoCodigo = new ArrayList<String>();
        // Cargo el alfabeto codigo
        alfabetoCodigo.add("0");
        alfabetoCodigo.add("1");
        // Obtengo el codigo
        assertEquals(obtenedor.armarCodigo(tamanioLongitudes, alfabetoFuente, alfabetoCodigo, listaProba), null);
    }

    @Test
    public void testObtenerCodigoInstantaneo2() {
        ObtenedorCodigoInstantaneo obtenedor = new ObtenedorCodigoInstantaneo();
        List<Integer> tamanioLongitudes = new ArrayList<Integer>();
        tamanioLongitudes.add(1);
        tamanioLongitudes.add(2);
        tamanioLongitudes.add(3);
        tamanioLongitudes.add(3);
        List<String> alfabetoFuente = new ArrayList<String>();
        // Cargo el alfabeto codigo
        alfabetoFuente.add("hola");
        alfabetoFuente.add("jamon");
        alfabetoFuente.add("lomo");
        alfabetoFuente.add("burra");
        List<Double> listaProba = new ArrayList<Double>();
        // Cargo las probabilidades
        listaProba.add(0.25);
        listaProba.add(0.25);
        listaProba.add(0.4);
        listaProba.add(0.1);
        List<String> alfabetoCodigo = new ArrayList<String>();
        // Cargo el alfabeto codigo
        alfabetoCodigo.add("0");
        alfabetoCodigo.add("1");
        // Obtengo el codigo
        CodigoBloque codigo = obtenedor.armarCodigo(tamanioLongitudes, alfabetoFuente,
                alfabetoCodigo, listaProba);
        List<String> palabrasFuentesCodigo = codigo.devolverAlfabetoFuente();
        List<String> palabrasCodigos = codigo.devolverPalabrasCodigos();
        assertEquals(palabrasFuentesCodigo.get(0), "lomo");
        assertEquals(palabrasCodigos.get(0), "0");
        assertEquals(palabrasFuentesCodigo.get(1), "hola");
        assertEquals(palabrasCodigos.get(1), "10");
        assertEquals(palabrasFuentesCodigo.get(2), "jamon");
        assertEquals(palabrasCodigos.get(2), "110");
        assertEquals(palabrasFuentesCodigo.get(3), "burra");
        assertEquals(palabrasCodigos.get(3), "111");
    }
}
