package elementosCodigo;

import java.util.List;

public class UnidadCodigoBloque {
	private String palabraFuente;
	private String palabraCodigo;
	private double probabilidad;

	public UnidadCodigoBloque(String simboloFuente, String palabraCodigo, double probabilidad) {
        this.palabraCodigo = palabraCodigo;
        this.palabraFuente = simboloFuente;
        this.probabilidad = probabilidad;
    }

	public boolean sonIgualesLosSimbolosFuentes(UnidadCodigoBloque otro) {
		return this.palabraFuente.equals(otro.palabraFuente);
	}

	public boolean sonIgualesLasPalabrasCodigos(UnidadCodigoBloque otro) {
		return this.palabraCodigo.equals(otro.palabraCodigo);
	}
	
	public boolean sonIgualesLasPalabrasCodigos(String otro) {
		return this.palabraCodigo.equals(otro);
	}
	
	public List<String> obtenerPrefijosPalabraCodigo(){
		ObtenedorDePrefijos obtenedor = new ObtenedorDePrefijos();
		return obtenedor.devolverPrefijos(palabraCodigo);
	}
	
	public double devolverProbabilidad(){
		return this.probabilidad;
	}
	
	public int devolverLongitudPalabraCodigo(){
		return this.palabraCodigo.length();
	}

    public String devolverPalabraCodigo() {
        return this.palabraCodigo;
    }
    
    public String devolverPalabraFuente() {
        return this.palabraFuente;
    }

}
