package interfazGrafica.ventanaCodigoCompleto;

import java.awt.EventQueue;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.JFrame;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;

import net.miginfocom.swing.MigLayout;

import javax.swing.SpringLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import controlador.Menu;
import controlador.MenuImplementacion;

import javax.swing.BoxLayout;

import java.awt.FlowLayout;

import javax.swing.JScrollPane;

import java.awt.BorderLayout;

import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaCodigoCompletoComparar {

	private JFrame frame;
	private ArrayList<Double> listaProbabilidades;
	private ArrayList<String> listaPalabrasCodigo;
	private ArrayList<String> listaAlfabetoFuente;
	private List<ArrayList<String>> listasAlfabetos;
	private List<ArrayList<String>> listasPalabras;
	private List<ArrayList<Double>> listasProbasCodigos;
	private String listaCodigo;
	private JTextField textAlfabeto;
	private JTextField textProbabilidad;
	private JTextField textField;
	private int numeroLista;


	/**
	 * Create the application.
	 */
	public VentanaCodigoCompletoComparar() {
		this.listaAlfabetoFuente= new ArrayList<String>();
		this.listaPalabrasCodigo= new ArrayList<String>();
		this.listaProbabilidades= new ArrayList<Double>();
		listasAlfabetos = new ArrayList<ArrayList<String>>();
		listasPalabras = new ArrayList<ArrayList<String>>();
		listasProbasCodigos = new ArrayList<ArrayList<Double>>();
		numeroLista = 1;
		listaCodigo="(vacio)";
		initialize();
		/** Para abrir TODO */
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 706, 438);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblCodigoBloque = new JLabel("Codigo");
		lblCodigoBloque.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblCodigoBloque.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblSimboloDelAlfabeto = new JLabel("Simbolo del alfabeto:");
		
		textAlfabeto = new JTextField();
		textAlfabeto.setColumns(10);
		
		JLabel lblProbabilidad = new JLabel("Probabilidad:");
		
		textProbabilidad = new JTextField();
		textProbabilidad.setColumns(10);
		
		JLabel lblPalabraCodigo = new JLabel("Palabra codigo:");
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JScrollPane scrollPane_1 = new JScrollPane();
		
		JTextArea textResultado = new JTextArea();
		scrollPane_1.setViewportView(textResultado);
		
		JTextArea textCodigoIngresado = new JTextArea();
		textCodigoIngresado.setText("(vacio)");
		scrollPane.setViewportView(textCodigoIngresado);
		
		JButton btnCalcularPropiedadesDel = new JButton("Calcular la eficiencia de los codigos");
		btnCalcularPropiedadesDel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Menu funciones = new MenuImplementacion();
				//Agrego la ultima lista que se estaba llenado.
				agregarListas();
				//String resultado = funciones.determinarCodigoMasEficiente(listasProbasCodigos, listasAlfabetos, listasPalabras);
				//textResultado.setText(resultado);
			}
		});
		
		JButton btnAgregarSimbolo = new JButton("Agregar simbolo");
		btnAgregarSimbolo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				listaAlfabetoFuente.add(textAlfabeto.getText());
				listaPalabrasCodigo.add(textField.getText());
				NumberFormat format = NumberFormat.getInstance(Locale.US);
				try {
					Number number = format.parse(textProbabilidad.getText());
					double numberDouble = number.doubleValue();
					listaProbabilidades.add(numberDouble);
					if (listaCodigo.contentEquals("(vacio)"))
						listaCodigo = "Lista numero 1:\n";
					listaCodigo += "( " + textAlfabeto.getText()+" ; " + textProbabilidad.getText() + " ; " + textField.getText() + " )\n";
					textCodigoIngresado.setText(listaCodigo);
				} catch (ParseException exception) {
				}
			}
		});
		
		JLabel lblCodigo = new JLabel("Codigo:");
		
		JLabel lblPropiedades = new JLabel("Propiedades:");
		
		JButton btnAgregarNuevoCodigo = new JButton("Agregar nuevo codigo");
		btnAgregarNuevoCodigo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				numeroLista++;
				listaCodigo += "Lista numero " + numeroLista + ":\n";
				textCodigoIngresado.setText(listaCodigo);
//				//Agrego las listas para calcular
//				listasAlfabetos.add(listaAlfabetoFuente);
//				listasPalabras.add(listaPalabrasCodigo);
//				listasProbasCodigos.add(listaProbabilidades);
				agregarListas();
				//Creo las nuevas listas
				listaAlfabetoFuente= new ArrayList<String>();
				listaPalabrasCodigo= new ArrayList<String>();
				listaProbabilidades= new ArrayList<Double>();
			}
		});
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/** Para abrir TODO */
				frame.setVisible(false);
			}
		});
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblCodigoBloque, GroupLayout.DEFAULT_SIZE, 661, Short.MAX_VALUE)
					.addGap(19))
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(24)
							.addComponent(lblSimboloDelAlfabeto)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(textAlfabeto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
							.addComponent(lblProbabilidad))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(28)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblCodigo)
									.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(btnAgregarSimbolo, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnAgregarNuevoCodigo, GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)))))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(4)
							.addComponent(textProbabilidad, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
							.addComponent(lblPalabraCodigo)
							.addGap(18)
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(15))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE)
								.addComponent(btnCalcularPropiedadesDel, GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE)
								.addComponent(lblPropiedades))))
					.addGap(13))
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addGap(331)
					.addComponent(btnVolver, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(296))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(18)
					.addComponent(lblCodigoBloque, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textProbabilidad, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblProbabilidad)
						.addComponent(lblSimboloDelAlfabeto)
						.addComponent(textAlfabeto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPalabraCodigo))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnCalcularPropiedadesDel)
						.addComponent(btnAgregarNuevoCodigo)
						.addComponent(btnAgregarSimbolo))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCodigo)
						.addComponent(lblPropiedades))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(scrollPane_1)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE))
					.addGap(18, 18, Short.MAX_VALUE)
					.addComponent(btnVolver)
					.addContainerGap())
		);
		
		frame.getContentPane().setLayout(groupLayout);
	}
	
	private void agregarListas() {
	  //Agrego las listas para calcular
        listasAlfabetos.add(listaAlfabetoFuente);
        listasPalabras.add(listaPalabrasCodigo);
        listasProbasCodigos.add(listaProbabilidades);
	}
}
