package elementosCodigo;

import java.util.List;

public class Kraft {

    /**
     * Devuelve un booleano indicando si se cumple o no Kraft con las condiciones pasadas.
     * 
     * @param tamanioLongitudes
     *            : lista de los tama�os codigos de un codigo.
     * @param n
     *            : es la cantidad de simbolos que existe en el alfabeto codigo.
     * @return true si cumple con Kraft, en caso contrario devuelve false.
     */
    public boolean cumple(List<Integer> tamanioLongitudes, int n) {
        double resultado = 0;
        for (Integer longitud : tamanioLongitudes) {
            resultado += 1 / Math.pow(n, (longitud));
        }
        return resultado <= 1;
    }

}
