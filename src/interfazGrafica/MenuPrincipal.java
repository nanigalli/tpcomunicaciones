package interfazGrafica;

import interfazGrafica.ventanaCodigoCompleto.VentanaCodigoCompletoComparar;
import interfazGrafica.ventanaCodigoCompleto.VentanaCodigoPropiedades;
import interfazGrafica.ventanaConstructorCodigo.VentanaConstructorCodigo;
import interfazGrafica.ventanaProbabilidadAlfabeto.VentanaProbabilidadAlfabeto;
import interfazGrafica.ventanasEntropia.CalculadorDeEntropia;
import interfazGrafica.ventanasEntropia.ComparadorDeEntropia;
import interfazGrafica.ventanasEntropia.VentanaDatosProbabilidadUnidadDevuelveEntropia;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.ButtonGroup;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingConstants;

public class MenuPrincipal {

	private JFrame frame;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipal window = new MenuPrincipal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MenuPrincipal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 688, 387);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblMenu = new JLabel("Menu");
		lblMenu.setHorizontalAlignment(SwingConstants.CENTER);
		lblMenu.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		JRadioButton rdbtnDeterminarEntropia = new JRadioButton("Determinar la entrop\u00EDa o falta de informaci\u00F3n asociada a una distribuci\u00F3n de probabilidades");
		buttonGroup.add(rdbtnDeterminarEntropia);
		
		JRadioButton rdbtnCompararEntropias = new JRadioButton("Compara la entrop\u00EDa asociada a una distribuci\u00F3n de probabilidades con la entrop\u00EDa de la equiprobabilidad");
		buttonGroup.add(rdbtnCompararEntropias);
		
//		JRadioButton rdbtnDeterminarQueCdigo = new JRadioButton("Determinar que c\u00F3digo es m\u00E1s eficiente entre otros");
//		buttonGroup.add(rdbtnDeterminarQueCdigo);
		
		JRadioButton rdbtnDeterminarLaLongitud = new JRadioButton("Determinar la longitud media de un c\u00F3digo");
		buttonGroup.add(rdbtnDeterminarLaLongitud);
		
		JRadioButton rdbtnDeterminarQuePropiedades = new JRadioButton("Determinar que propiedades cumple un codigo");
		buttonGroup.add(rdbtnDeterminarQuePropiedades);
		
		JRadioButton rdbtnDevolverUnCdigo = new JRadioButton("Devolver un c\u00F3digo bloque instant\u00E1neo a partir de ciertas condiciones");
		buttonGroup.add(rdbtnDevolverUnCdigo);
		
		JButton btnEjecutar = new JButton("Ejecutar");
		btnEjecutar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (rdbtnDeterminarEntropia.isSelected()){
					VentanaDatosProbabilidadUnidadDevuelveEntropia ventana = new VentanaDatosProbabilidadUnidadDevuelveEntropia(new CalculadorDeEntropia());
				} else {
					if (rdbtnCompararEntropias.isSelected()){
						VentanaDatosProbabilidadUnidadDevuelveEntropia ventana = new VentanaDatosProbabilidadUnidadDevuelveEntropia(new ComparadorDeEntropia());
					} else {
//						if (rdbtnDeterminarQueCdigo.isSelected()){
//							VentanaCodigoCompletoComparar ventana = new VentanaCodigoCompletoComparar();
//						} else {
							if (rdbtnDeterminarLaLongitud.isSelected()) {
								VentanaProbabilidadAlfabeto ventana= new VentanaProbabilidadAlfabeto();
							} else {
								if (rdbtnDeterminarQuePropiedades.isSelected()) {
									VentanaCodigoPropiedades ventana = new VentanaCodigoPropiedades();
								} else {
									if (rdbtnDevolverUnCdigo.isSelected()){
									    VentanaConstructorCodigo ventana = new VentanaConstructorCodigo();
									}
								}
							}
//						}
					}
				}
			}
		});
		btnEjecutar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(197)
							.addComponent(btnEjecutar)
							.addGap(289))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblMenu, GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
							.addContainerGap())))
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(32)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(rdbtnCompararEntropias)
						.addComponent(rdbtnDeterminarEntropia)
//						.addComponent(rdbtnDeterminarQueCdigo)
						.addComponent(rdbtnDeterminarLaLongitud)
						.addComponent(rdbtnDeterminarQuePropiedades)
						.addComponent(rdbtnDevolverUnCdigo))
					.addContainerGap(117, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(28)
					.addComponent(lblMenu, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(rdbtnDeterminarEntropia)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(rdbtnCompararEntropias)
					.addPreferredGap(ComponentPlacement.UNRELATED)
//					.addComponent(rdbtnDeterminarQueCdigo)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(rdbtnDeterminarLaLongitud)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(rdbtnDeterminarQuePropiedades)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(rdbtnDevolverUnCdigo)
					.addGap(18)
					.addComponent(btnEjecutar)
					.addContainerGap(80, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
