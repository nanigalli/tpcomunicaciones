package interfazGrafica.ventanasEntropia;

import java.util.List;

import javax.swing.JTextPane;

import controlador.Menu;
import controlador.MenuImplementacion;

public class CalculadorDeEntropia implements Calculable{

	@Override
	public void calcular(List<Double> listaProbabilidades, String unidad, JTextPane resultadoEntropia) {
		Menu funciones = new MenuImplementacion();
//		String resultado = "La entropia del codigo es: ";
//		resultado += funciones.calcularEntropia(listaProbabilidades, unidad)+ unidad;
		String resultado = funciones.calcularEntropia(listaProbabilidades, unidad);
		resultadoEntropia.setText(resultado);
	}

}
