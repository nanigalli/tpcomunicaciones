package elementosCodigo;

import java.util.ArrayList;
import java.util.List;

public class ObtenedorDePrefijos {

	public List<String> devolverPrefijos(String palabra) {
		List<String> prefijos = new ArrayList<String>();
		int i;
		for (i = 1; i < palabra.length(); i++) {
			String prefijo = palabra.substring(0, i);
			prefijos.add(prefijo);
		}
		return prefijos;
	}
	
}
