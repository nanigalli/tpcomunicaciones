package interfazGrafica.ventanaProbabilidadAlfabeto;

import interfazGrafica.ventanaError.VentanaError;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.GridLayout;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import javax.swing.JEditorPane;
import javax.swing.JButton;

import controlador.Menu;
import controlador.MenuImplementacion;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.awt.Font;
import javax.swing.SwingConstants;

public class VentanaProbabilidadAlfabeto {

	private JFrame frame;
	private JTextField textSimbolo;
	private JTextField textProbabilidad;
	private List<Double> listaProbabilidades;
	private List<String> alfabeto;
	private String listaProbaAlfabetoTexto;

	/**
	 * Create the application.
	 */
	public VentanaProbabilidadAlfabeto() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		listaProbaAlfabetoTexto="(vacio)";
		listaProbabilidades= new ArrayList<Double>();
		alfabeto= new ArrayList<String>();
		frame = new JFrame();
		frame.setBounds(100, 100, 442, 409);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JLabel lblNewLabel = new JLabel("Calculo de la longitud media");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel, 438, SpringLayout.WEST, frame.getContentPane());
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 17));
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblSimboloDelAlfabeto = new JLabel("Palabra codigo:");
		springLayout.putConstraint(SpringLayout.NORTH, lblSimboloDelAlfabeto, 42, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblSimboloDelAlfabeto, 10, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblSimboloDelAlfabeto);
		
		textSimbolo = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textSimbolo, -3, SpringLayout.NORTH, lblSimboloDelAlfabeto);
		springLayout.putConstraint(SpringLayout.WEST, textSimbolo, 6, SpringLayout.EAST, lblSimboloDelAlfabeto);
		springLayout.putConstraint(SpringLayout.EAST, textSimbolo, 206, SpringLayout.EAST, lblSimboloDelAlfabeto);
		frame.getContentPane().add(textSimbolo);
		textSimbolo.setColumns(10);
		
		JLabel lblProbabilidad = new JLabel("Probabilidad:");
		springLayout.putConstraint(SpringLayout.NORTH, lblProbabilidad, 12, SpringLayout.SOUTH, lblSimboloDelAlfabeto);
		springLayout.putConstraint(SpringLayout.WEST, lblProbabilidad, 0, SpringLayout.WEST, lblNewLabel);
		frame.getContentPane().add(lblProbabilidad);
		
		textProbabilidad = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textProbabilidad, 6, SpringLayout.SOUTH, textSimbolo);
		springLayout.putConstraint(SpringLayout.WEST, textProbabilidad, 18, SpringLayout.EAST, lblProbabilidad);
		springLayout.putConstraint(SpringLayout.EAST, textProbabilidad, 218, SpringLayout.EAST, lblProbabilidad);
		frame.getContentPane().add(textProbabilidad);
		textProbabilidad.setColumns(10);
		
		JTextPane textAlfabetoProba = new JTextPane();
		springLayout.putConstraint(SpringLayout.WEST, textAlfabetoProba, 10, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(textAlfabetoProba);
		
		JLabel lblLongitudMedia = new JLabel("Longitud media:");
		springLayout.putConstraint(SpringLayout.WEST, lblLongitudMedia, 212, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblLongitudMedia, -34, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(lblLongitudMedia);
		
		JTextPane textLongitud = new JTextPane();
		springLayout.putConstraint(SpringLayout.SOUTH, lblLongitudMedia, -6, SpringLayout.NORTH, textLongitud);
		springLayout.putConstraint(SpringLayout.NORTH, textAlfabetoProba, 0, SpringLayout.NORTH, textLongitud);
		springLayout.putConstraint(SpringLayout.SOUTH, textAlfabetoProba, 0, SpringLayout.SOUTH, textLongitud);
		springLayout.putConstraint(SpringLayout.EAST, textAlfabetoProba, -6, SpringLayout.WEST, textLongitud);
		springLayout.putConstraint(SpringLayout.NORTH, textLongitud, 120, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, textLongitud, 212, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, textLongitud, 392, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(textLongitud);
		
		JButton btnAgregarsimbolo = new JButton("Agregar");
		springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel, -7, SpringLayout.NORTH, btnAgregarsimbolo);
		springLayout.putConstraint(SpringLayout.WEST, btnAgregarsimbolo, 6, SpringLayout.EAST, textSimbolo);
		springLayout.putConstraint(SpringLayout.EAST, btnAgregarsimbolo, 106, SpringLayout.EAST, textSimbolo);
		springLayout.putConstraint(SpringLayout.NORTH, btnAgregarsimbolo, -4, SpringLayout.NORTH, lblSimboloDelAlfabeto);
		btnAgregarsimbolo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			    String simbolo = textSimbolo.getText();
			    if (estaVacio(simbolo)) {
                    new VentanaError("La palabra codigo esta vacia, introduzca un valor.");
                    return;
                }
			    if (alfabeto.contains(simbolo)) {
			        new VentanaError("La palabra codigo ya fue ingresada, introduzca un nuevo valor.");
                    return;
			    }
				NumberFormat format = NumberFormat.getInstance(Locale.US);
				try {
					Number number = format.parse(textProbabilidad.getText());
					double numberDouble = number.doubleValue();
					if (numberDouble <= 0) {
                        new VentanaError("La probabilidad debe ser mayor a 0.");
                        return;
                    }
                    if (numberDouble >= 1) {
                        new VentanaError("La probabilidad debe ser menor a 1.");
                        return;
                    }
					alfabeto.add(simbolo);
					listaProbabilidades.add(numberDouble);
					if (listaProbaAlfabetoTexto.contentEquals("(vacio)"))
						listaProbaAlfabetoTexto = "";
					listaProbaAlfabetoTexto += "( " + textSimbolo.getText()+" ; " + textProbabilidad.getText() + " )\n";
					textAlfabetoProba.setText(listaProbaAlfabetoTexto);
				} catch (ParseException exception) {
				    new VentanaError("La probabilidad debe ser un valor numerico entre 0 y 1.");
                    return;
				}
			}
		});
		frame.getContentPane().add(btnAgregarsimbolo);
		
		JButton btnCalcular = new JButton("Calcular");
		springLayout.putConstraint(SpringLayout.NORTH, lblLongitudMedia, 6, SpringLayout.SOUTH, btnCalcular);
		springLayout.putConstraint(SpringLayout.NORTH, btnCalcular, -4, SpringLayout.NORTH, lblProbabilidad);
		springLayout.putConstraint(SpringLayout.WEST, btnCalcular, 6, SpringLayout.EAST, textProbabilidad);
		springLayout.putConstraint(SpringLayout.EAST, btnCalcular, 106, SpringLayout.EAST, textProbabilidad);
		btnCalcular.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Menu funciones = new MenuImplementacion();
				String resultado = "";
				resultado += funciones.determinarLongitudMedia(alfabeto, listaProbabilidades);
				textLongitud.setText(resultado);
			}
		});
		frame.getContentPane().add(btnCalcular);
		
		JButton btnVolverAlMenu = new JButton("Volver");
		springLayout.putConstraint(SpringLayout.SOUTH, textLongitud, -5, SpringLayout.NORTH, btnVolverAlMenu);
		springLayout.putConstraint(SpringLayout.SOUTH, btnVolverAlMenu, -10, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, btnVolverAlMenu, -33, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, btnVolverAlMenu, 131, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnVolverAlMenu, -124, SpringLayout.EAST, frame.getContentPane());
		btnVolverAlMenu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/** Para cerrar TODO */
				frame.setVisible(false);
			}
		});
		frame.getContentPane().add(btnVolverAlMenu);
		
		JLabel lblNewLabel_1 = new JLabel("Datos ingresados:");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_1, 6, SpringLayout.SOUTH, textProbabilidad);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_1, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel_1, 29, SpringLayout.SOUTH, textProbabilidad);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_1, -6, SpringLayout.WEST, lblLongitudMedia);
		frame.getContentPane().add(lblNewLabel_1);
		/** Para abrir TODO */
		frame.setVisible(true);
	}
	
	private boolean estaVacio(String palabra) {
        if (palabra == null) {
            return true;
        }

        return palabra.trim().isEmpty();
    }
}
