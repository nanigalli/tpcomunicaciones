import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import controlador.MenuImplementacion;
import elementosCodigo.CodigoBloque;

public class MenuImplementacionTest {
	
	private MenuImplementacion unMenu;
	private ArrayList<String> alfabetoUno;
	private ArrayList<String> palabrasCodigoUno;
	private ArrayList<Double> probabilidadesUno;
	
	@Before
    public void setUp() {
		unMenu = new MenuImplementacion();
		alfabetoUno = new ArrayList<String>(Arrays.asList("A","B","C"));
		probabilidadesUno = new ArrayList<Double>(Arrays.asList(0.5,0.25,0.25));
		palabrasCodigoUno = new ArrayList<String>(Arrays.asList("can", "come", "caca"));
    }
	
//	@Test
//	public void testDeterminarCodigoMasEficiente() {
//	
//		ArrayList<String> alfabetoDos = new ArrayList<String>(Arrays.asList("D","E","F"));
//		ArrayList<String> alfabetoTres = new ArrayList<String>(Arrays.asList("G","H","I")); 
//		
//		ArrayList<Double> probabilidadesDos = new ArrayList<Double>(Arrays.asList(0.3,0.3,0.4));
//		ArrayList<Double> probabilidadesTres = new ArrayList<Double>(Arrays.asList(0.1,0.45,0.45));
//		
//		ArrayList<String> palabrasCodigoDos = new ArrayList<String>(Arrays.asList("00", "01", "10"));
//		ArrayList<String> palabrasCodigoTres = new ArrayList<String>(Arrays.asList(".", "_", ".."));
//		
//		ArrayList<ArrayList<String>> listasAlfabetos 
//			= new ArrayList<ArrayList<String>>(Arrays.asList(alfabetoUno,alfabetoDos,alfabetoTres));
//		
//		ArrayList<ArrayList<Double>> listasProbasCodigos 
//		= new ArrayList<ArrayList<Double>>(Arrays.asList(probabilidadesUno,probabilidadesDos,probabilidadesTres));
//		
//		ArrayList<ArrayList<String>> listasPalabrasCodigo 
//		= new ArrayList<ArrayList<String>>(Arrays.asList(palabrasCodigoUno,palabrasCodigoDos,palabrasCodigoTres));
//		
//		//Creo los codigos bloques
//		
//		CodigoBloque unCodigoBloqueUno = new CodigoBloque();
//		CodigoBloque unCodigoBloqueDos = new CodigoBloque();
//		CodigoBloque unCodigoBloqueTres = new CodigoBloque();
//		for(int numeroElemento = 0; numeroElemento < alfabetoTres.size(); numeroElemento ++) {
//		    unCodigoBloqueUno.agregarUnidad(alfabetoUno.get(numeroElemento), palabrasCodigoUno.get(numeroElemento), probabilidadesUno.get(numeroElemento));
//		    unCodigoBloqueDos.agregarUnidad(alfabetoDos.get(numeroElemento), palabrasCodigoDos.get(numeroElemento), probabilidadesDos.get(numeroElemento));
//            unCodigoBloqueTres.agregarUnidad(alfabetoTres.get(numeroElemento), palabrasCodigoTres.get(numeroElemento), probabilidadesTres.get(numeroElemento));
//        }
//		double eficienciaUno = (unCodigoBloqueUno.devolverEntropia(2) / unCodigoBloqueUno.devolverLongitudMedia());
//		double eficienciaDos = (unCodigoBloqueDos.devolverEntropia(2) / unCodigoBloqueDos.devolverLongitudMedia());
//		double eficienciaTres = (unCodigoBloqueTres.devolverEntropia(2) / unCodigoBloqueTres.devolverLongitudMedia());
//		
//		System.out.println("Eficiencia 1: "+eficienciaUno);
//		System.out.println("Eficiencia 2: "+eficienciaDos);
//		System.out.println("Eficiencia 3: "+eficienciaTres);
//		
//		List<String> listaAlfabetoCodigo = new ArrayList<>();
//		
//		
//		String resultado = unMenu.determinarCodigoMasEficiente(listasProbasCodigos, listasAlfabetos, listasPalabrasCodigo, listaAlfabetoCodigo);
//		System.out.println(resultado);
//		assertEquals(resultado, "El c�digo bloque m�s eficiente es el n�mero 3 con un rendimiento de "+eficienciaTres+".");
//	
//	}
	
	@Test
	public void calcularEntropia(){
	    String entropia = unMenu.calcularEntropia(probabilidadesUno, "bits");
	    //System.out.println(entropia);
	    assertEquals(entropia, "1.5 bits");
	}

    public void compararEntropiaConUnaDistribucionEquiprobable(){
        
    }
         
    @Test
    public void determinarLongitudMedia(){
        String resultado = unMenu.determinarLongitudMedia(palabrasCodigoUno, probabilidadesUno);
        //System.out.println(resultado);
        assertEquals(resultado, "Las longitudes por palabra codigo son: \n"+"  can -> 3\n"+"  caca -> 4\n"+"  come -> 4\n"+
                "\n La longitud media del codigo es 3.5.");
    }

}
