

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import elementosCodigo.ObtenedorDePrefijos;


public class ObtenedorDePrefijosTest {

	@Test
	public void testObtenerPrefijo(){
		ObtenedorDePrefijos obtenedor = new ObtenedorDePrefijos();
		List<String> prefijos = obtenedor.devolverPrefijos("choco");
		List<String> prefijosCalculados = new ArrayList<String>();
		prefijosCalculados.add("c");
		prefijosCalculados.add("ch");
		prefijosCalculados.add("cho");
		prefijosCalculados.add("choc");
		assertTrue(prefijos.containsAll(prefijosCalculados));
	}
}
