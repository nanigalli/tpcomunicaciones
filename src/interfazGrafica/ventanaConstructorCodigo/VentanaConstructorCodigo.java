package interfazGrafica.ventanaConstructorCodigo;

import interfazGrafica.ventanaError.VentanaError;

import java.awt.EventQueue;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.JFrame;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;

import net.miginfocom.swing.MigLayout;

import javax.swing.SpringLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import controlador.Menu;
import controlador.MenuImplementacion;

import javax.swing.BoxLayout;

import java.awt.FlowLayout;

import javax.swing.JScrollPane;

import java.awt.BorderLayout;

import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaConstructorCodigo {

	private JFrame frame;
	private ArrayList<Integer> listaLongitudes;
	private ArrayList<Double> listaProbabilidad;
	private ArrayList<String> listaAlfabetoCodigo;
	private ArrayList<String> listaAlfabetoFuente;
	private ArrayList<String> listaImpresionFuenteProba;
	private JTextField simboloCodigoIngresado;
	private JTextField textProbabilidad;
	private JTextField palabraFuenteIngresada;
	private JTextField probabilidad;


	/**
	 * Create the application.
	 */
	public VentanaConstructorCodigo() {
		this.listaAlfabetoFuente= new ArrayList<String>();
		this.listaAlfabetoCodigo= new ArrayList<String>();
		this.listaLongitudes= new ArrayList<Integer>();
		this.listaProbabilidad = new ArrayList<Double>();
		listaImpresionFuenteProba = new ArrayList<String>();
		initialize();
		/** Para abrir TODO */
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 734, 438);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblCodigoBloque = new JLabel("Constructor de codigos instantaneos");
		lblCodigoBloque.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblCodigoBloque.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblSimboloDelAlfabeto = new JLabel("Simbolo del alfabeto codigo:");
		
		simboloCodigoIngresado = new JTextField();
		simboloCodigoIngresado.setColumns(10);
		
		JLabel lblProbabilidad = new JLabel("Longitud:");
		
		textProbabilidad = new JTextField();
		textProbabilidad.setColumns(10);
		
		JLabel lblPalabraCodigo = new JLabel("Palabra fuente:");
		
		palabraFuenteIngresada = new JTextField();
		palabraFuenteIngresada.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JScrollPane scrollPane_1 = new JScrollPane();
		
		JTextArea textResultado = new JTextArea();
		scrollPane_1.setViewportView(textResultado);
		
		JTextArea textCodigoIngresado = new JTextArea();
		textCodigoIngresado.setText("(vacio)");
		scrollPane.setViewportView(textCodigoIngresado);
		
		JButton btnCalcularPropiedadesDel = new JButton("Calcular codigo");
		btnCalcularPropiedadesDel.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    }
		});
		btnCalcularPropiedadesDel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Menu funciones = new MenuImplementacion();
				if (listaAlfabetoFuente.size() == 0) {
				    new VentanaError("No se ingreso el alfabeto fuente, ingreselo y vuelva a calcular.");
                    return;
				}
				if (listaAlfabetoCodigo.size() == 0) {
                    new VentanaError("No se ingreso el alfabeto codigo, ingreselo y vuelva a calcular.");
                    return;
                }
				if (listaLongitudes.size() == 0) {
                    new VentanaError("No se ingreso las longitudes, ingreselas y vuelva a calcular.");
                    return;
                }
				String resultado = funciones.devolverCodigoInstantaneo(listaAlfabetoFuente, listaAlfabetoCodigo, listaLongitudes, listaProbabilidad);
				textResultado.setText(resultado);
			}
		});
		
		JButton btnAgregarSimbolo = new JButton("Agregar simbolo al alfabeto codigo");
		btnAgregarSimbolo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			    String simboloAlfabeto;
                try {
                    simboloAlfabeto = simboloCodigoIngresado.getText();
                } catch (Exception e1) {
                    new VentanaError("No se ingreso un simbolo del alfabeto codigo.");
                    return;
                }
                simboloAlfabeto = simboloAlfabeto == null ? "" : simboloAlfabeto;
                if (simboloAlfabeto.trim().isEmpty()){
                    new VentanaError("Simbolo de alfabeto ingresado es vacio.");
                    return;
                }
                if (simboloAlfabeto.length() != 1) {
                    new VentanaError("El simbolo del alfabeto codigo debe solo contener un caracter.");
                    return;
                }
			    if (listaAlfabetoCodigo.contains(simboloAlfabeto)) {
			        new VentanaError("El simbolo del alfabeto codigo ingresada ya fue ingresada, pruebe con otro simbolo.");
			        return;
			    }
				listaAlfabetoCodigo.add(simboloAlfabeto);
				textCodigoIngresado.setText(obtenerInformacion());
			}
		});
		
		JLabel lblCodigo = new JLabel("Informacion agregada:");
		
		JLabel lblPropiedades = new JLabel("Codigo:");
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/** Para abrir TODO */
				frame.setVisible(false);
			}
		});
		
		JButton btnNewButton = new JButton("Agregar longitud");
		btnNewButton.addMouseListener(new MouseAdapter() {
		    @Override
		    public void mouseClicked(MouseEvent e) {
		        NumberFormat format = NumberFormat.getInstance(Locale.US);
                try {
                    Number number = format.parse(textProbabilidad.getText());
                    int numberInt = number.intValue();
                    if (numberInt<0) {
                        new VentanaError("La longitud debe ser un numero entero mayor a 0.");
                        return;
                    }
                    listaLongitudes.add(numberInt);
                    textCodigoIngresado.setText(obtenerInformacion());
                } catch (ParseException exception) {
                    new VentanaError("La longitud que se ingreso no es un valor numerico o no se ingreso un valor.");
                    return;
                }
		    }
		});
		
		JButton btnNewButton_1 = new JButton("Agregar palabra fuente");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
		    @Override
		    public void mouseClicked(MouseEvent e) {
		        Double proba;
                try {
                    proba = Double.parseDouble(probabilidad.getText());
                } catch (NumberFormatException e1) {
                    new VentanaError("No se ingreso una probabilidad o no es un valor numerico.");
                    return;
                }
		        String palabraFuente;
                try {
                    palabraFuente = palabraFuenteIngresada.getText();
                } catch (Exception e1) {
                    new VentanaError("No se ingreso una palabra fuente.");
                    return;
                }
                palabraFuente = palabraFuente == null ? "" : palabraFuente;
                if (palabraFuente.trim().isEmpty()){
                    new VentanaError("Simbolo de alfabeto ingresado es vacio.");
                    return;
                }
		        if (proba<=0) {
                    new VentanaError("La probabilidad debe ser un numero mayor a 0.");
                    return;
                }
		        if (proba>=1) {
                    new VentanaError("La probabilidad debe ser un numero menor a 1.");
                    return;
                }
		        if (listaAlfabetoFuente.contains(palabraFuente)) {
		            new VentanaError("La palabra fuente ingresada ya fue ingresada, pruebe con otra palabra.");
                    return;
                }
		        listaAlfabetoFuente.add(palabraFuente);
		        listaProbabilidad.add(proba);
		        listaImpresionFuenteProba.add(palabraFuente+" con probabilidad: "+proba);
		        textCodigoIngresado.setText(obtenerInformacion());
		    }
		});
		
		probabilidad = new JTextField();
		probabilidad.setColumns(10);
		
		JLabel lblProbabilidadDelSimbolo = new JLabel("Probabilidad del simbolo:");
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
		    groupLayout.createParallelGroup(Alignment.TRAILING)
		        .addGroup(groupLayout.createSequentialGroup()
		            .addGap(331)
		            .addComponent(btnVolver, GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
		            .addGap(296))
		        .addGroup(groupLayout.createSequentialGroup()
		            .addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
		                .addGroup(groupLayout.createSequentialGroup()
		                    .addContainerGap()
		                    .addComponent(lblCodigoBloque, GroupLayout.PREFERRED_SIZE, 651, GroupLayout.PREFERRED_SIZE))
		                .addGroup(groupLayout.createSequentialGroup()
		                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
		                        .addGroup(groupLayout.createSequentialGroup()
		                            .addGap(24)
		                            .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
		                                .addComponent(btnNewButton_1, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
		                                .addGroup(groupLayout.createSequentialGroup()
		                                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
		                                        .addComponent(lblProbabilidadDelSimbolo)
		                                        .addComponent(lblPalabraCodigo))
		                                    .addPreferredGap(ComponentPlacement.RELATED)
		                                    .addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
		                                        .addComponent(palabraFuenteIngresada, GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
		                                        .addComponent(probabilidad, GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE))
		                                    .addGap(4))))
		                        .addGroup(groupLayout.createSequentialGroup()
		                            .addGap(28)
		                            .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
		                                .addComponent(scrollPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
		                                .addGroup(groupLayout.createSequentialGroup()
		                                    .addComponent(lblCodigo)
		                                    .addPreferredGap(ComponentPlacement.RELATED, 142, Short.MAX_VALUE)))))
		                    .addPreferredGap(ComponentPlacement.RELATED)
		                    .addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
		                        .addGroup(groupLayout.createSequentialGroup()
		                            .addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
		                                .addComponent(btnCalcularPropiedadesDel, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
		                                .addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
		                                    .addGap(14)
		                                    .addComponent(lblProbabilidad)
		                                    .addPreferredGap(ComponentPlacement.RELATED)
		                                    .addComponent(textProbabilidad, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
		                                    .addPreferredGap(ComponentPlacement.RELATED)))
		                            .addGap(8))
		                        .addGroup(groupLayout.createSequentialGroup()
		                            .addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
		                            .addPreferredGap(ComponentPlacement.RELATED)))
		                    .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
		                        .addComponent(lblPropiedades)
		                        .addGroup(groupLayout.createSequentialGroup()
		                            .addPreferredGap(ComponentPlacement.RELATED)
		                            .addComponent(lblSimboloDelAlfabeto)
		                            .addPreferredGap(ComponentPlacement.RELATED)
		                            .addComponent(simboloCodigoIngresado, GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE))
		                        .addComponent(btnAgregarSimbolo, GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
		                        .addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE))))
		            .addGap(19))
		);
		groupLayout.setVerticalGroup(
		    groupLayout.createParallelGroup(Alignment.LEADING)
		        .addGroup(groupLayout.createSequentialGroup()
		            .addGap(18)
		            .addComponent(lblCodigoBloque, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
		            .addGap(2)
		            .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
		                .addComponent(lblProbabilidadDelSimbolo)
		                .addComponent(probabilidad, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
		            .addPreferredGap(ComponentPlacement.RELATED)
		            .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
		                .addComponent(lblPalabraCodigo)
		                .addGroup(groupLayout.createSequentialGroup()
		                    .addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
		                        .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
		                            .addComponent(textProbabilidad, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
		                            .addComponent(simboloCodigoIngresado, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
		                            .addComponent(palabraFuenteIngresada, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
		                            .addComponent(lblProbabilidad))
		                        .addComponent(lblSimboloDelAlfabeto))
		                    .addPreferredGap(ComponentPlacement.UNRELATED)
		                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
		                        .addComponent(btnAgregarSimbolo)
		                        .addComponent(btnNewButton_1)
		                        .addComponent(btnNewButton))))
		            .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
		                .addGroup(groupLayout.createSequentialGroup()
		                    .addGap(6)
		                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
		                        .addComponent(lblCodigo)
		                        .addComponent(lblPropiedades))
		                    .addPreferredGap(ComponentPlacement.RELATED)
		                    .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
		                        .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
		                        .addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)))
		                .addGroup(groupLayout.createSequentialGroup()
		                    .addGap(77)
		                    .addComponent(btnCalcularPropiedadesDel)))
		            .addPreferredGap(ComponentPlacement.RELATED)
		            .addComponent(btnVolver)
		            .addGap(6))
		);
		
		frame.getContentPane().setLayout(groupLayout);
	}
	
	private String obtenerInformacion() {
	    String informacion = "";
	    if(listaAlfabetoCodigo.size()>0) {
	        informacion += "Alfabeto codigo: \n";
	        for (String palabra : listaAlfabetoCodigo) {
	            informacion += "      "+palabra+"\n";
	        }
	        informacion +="\n";
	    }
	    if(listaImpresionFuenteProba.size()>0) {
            informacion += "Alfabeto fuente: \n";
            for (String palabra : listaImpresionFuenteProba) {
                informacion += "      "+palabra+"\n";
            }
            informacion +="\n";
        }
	    if(listaLongitudes.size()>0) {
            informacion += "Longitudes: \n";
            for (Integer palabra : listaLongitudes) {
                informacion += "      "+palabra+"\n";
            }
        }
	    if(informacion.equals(""))
	        return "(vacio)";
	    return informacion;
	}
}
