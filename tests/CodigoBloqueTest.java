import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import elementosCodigo.CodigoBloque;

public class CodigoBloqueTest {

    private static final double DELTA = 0.00001;

    @Test
    public void testCodigoSingular() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad("a", "b", 0);
        codigo.agregarUnidad("x", "y", 0);
        codigo.agregarUnidad("r", "b", 0);
        assertTrue(codigo.esSigular());
    }

    @Test
    public void testCodigoSingularConLaInformacionNecesaria() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad(null, "b", 0);
        codigo.agregarUnidad(null, "y", 0);
        codigo.agregarUnidad(null, "b", 0);
        assertTrue(codigo.esSigular());
    }

    @Test
    public void testCodigoNoSingular() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad("a", "b", 0);
        codigo.agregarUnidad("x", "y", 0);
        codigo.agregarUnidad("r", "e", 0);
        assertFalse(codigo.esSigular());
    }

    @Test
    public void testCodigoInstantaneo() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad("a", "baba", 0);
        codigo.agregarUnidad("x", "casa", 0);
        codigo.agregarUnidad("r", "bo", 0);
        List<String> alfabetoCodigo = new ArrayList<String>();
        alfabetoCodigo.add("b");
        alfabetoCodigo.add("a");
        alfabetoCodigo.add("c");
        alfabetoCodigo.add("o");
        alfabetoCodigo.add("s");
        codigo.agregarAlfabetoCodigo(alfabetoCodigo);
        assertTrue(codigo.esInstantaneo());
    }

    @Test
    public void testCodigoInstantaneoConLaInformacionNecesaria() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad(null, "baba", 0);
        codigo.agregarUnidad(null, "casa", 0);
        codigo.agregarUnidad(null, "bo", 0);
        List<String> alfabetoCodigo = new ArrayList<String>();
        alfabetoCodigo.add("b");
        alfabetoCodigo.add("a");
        alfabetoCodigo.add("c");
        alfabetoCodigo.add("o");
        alfabetoCodigo.add("s");
        codigo.agregarAlfabetoCodigo(alfabetoCodigo);
        assertTrue(codigo.esInstantaneo());
    }

    @Test
    public void testCodigoNoInstantaneo() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad("a", "baba", 0);
        codigo.agregarUnidad("x", "casa", 0);
        codigo.agregarUnidad("r", "ba", 0);
        List<String> alfabetoCodigo = new ArrayList<String>();
        alfabetoCodigo.add("b");
        alfabetoCodigo.add("a");
        alfabetoCodigo.add("c");
        alfabetoCodigo.add("o");
        alfabetoCodigo.add("s");
        codigo.agregarAlfabetoCodigo(alfabetoCodigo);
        assertFalse(codigo.esInstantaneo());
    }

    @Test
    public void testCodigoNoInstantaneoPorqueEsSingular() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad("a", "b", 0);
        codigo.agregarUnidad("x", "y", 0);
        codigo.agregarUnidad("r", "b", 0);
        List<String> alfabetoCodigo = new ArrayList<String>();
        alfabetoCodigo.add("b");
        alfabetoCodigo.add("a");
        alfabetoCodigo.add("c");
        alfabetoCodigo.add("y");
        alfabetoCodigo.add("s");
        codigo.agregarAlfabetoCodigo(alfabetoCodigo);
        assertFalse(codigo.esInstantaneo());
    }

    @Test
    public void testCodigoNoInstantaneoPorqueHayPrefijos() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad("a", "ba", 0);
        codigo.agregarUnidad("x", "y", 0);
        codigo.agregarUnidad("r", "b", 0);
        List<String> alfabetoCodigo = new ArrayList<String>();
        alfabetoCodigo.add("b");
        alfabetoCodigo.add("a");
        alfabetoCodigo.add("y");
        codigo.agregarAlfabetoCodigo(alfabetoCodigo);
        assertFalse(codigo.esInstantaneo());
    }

    @Test
    public void testCodigoNoInstantaneoPorqueHayPrefijos2() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad("a", "b", 0);
        codigo.agregarUnidad("x", "y", 0);
        codigo.agregarUnidad("r", "ba", 0);
        List<String> alfabetoCodigo = new ArrayList<String>();
        alfabetoCodigo.add("b");
        alfabetoCodigo.add("a");
        alfabetoCodigo.add("y");
        codigo.agregarAlfabetoCodigo(alfabetoCodigo);
        assertFalse(codigo.esInstantaneo());
    }

    @Test
    public void testLongitudMedia() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad(null, "baba", 1 / 2);
        codigo.agregarUnidad(null, "casa", 1 / 4);
        codigo.agregarUnidad(null, "ba", 1 / 4);
        assertEquals(codigo.devolverLongitudMedia(), 3, 5);
    }

    @Test
    public void testEntropiaEnBase2() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad(null, "baba", 0.5);
        codigo.agregarUnidad(null, "casa", 0.25);
        codigo.agregarUnidad(null, "ba", 0.25);
        float res = (float) ((Math.log(4) / Math.log(2)) * 2 / 4 + (Math.log(2) / Math.log(2)) / 2);
        assertEquals(res, codigo.devolverEntropia(2), DELTA);
    }

    // La distribución de probabilidades de N eventos tiene una entropía menor
    // que la falta de información que la distribución de probabilidades con N eventos.

    @Test
    public void testEntropiaEnBase2_ACGT_NotEquiprobable() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad(null, "A", 0.5);
        codigo.agregarUnidad(null, "C", 0.25);
        codigo.agregarUnidad(null, "G", 0.125);
        codigo.agregarUnidad(null, "T", 0.125);
        float resultadoEsperado = (float) 1.75;
        assertEquals(resultadoEsperado, codigo.devolverEntropia(2), DELTA);
    }

    @Test
    public void testEntropiaEnBase2_ACGT_Equiprobable() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad(null, "A", 0.25);
        codigo.agregarUnidad(null, "C", 0.25);
        codigo.agregarUnidad(null, "G", 0.25);
        codigo.agregarUnidad(null, "T", 0.25);
        float resultadoEsperado = (float) 2.0;
        assertEquals(resultadoEsperado, codigo.devolverEntropia(2), DELTA);
    }

    @Test
    public void testEntropiaEnBase10() {
        CodigoBloque codigo = new CodigoBloque();
        codigo.agregarUnidad(null, "baba", 0.5);
        codigo.agregarUnidad(null, "casa", 0.25);
        codigo.agregarUnidad(null, "ba", 0.25);
        float res = (float) ((Math.log(4) / Math.log(10)) * 2 / 4 + (Math.log(2) / Math.log(10)) / 2);
        assertEquals(codigo.devolverEntropia(10), res, DELTA);
    }

}
